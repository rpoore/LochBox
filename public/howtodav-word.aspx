﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" Runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            // hide the header logout button here
            $('#login-info').hide();
            $('#username-label-header-icon').hide();
            $('#buttonStuff').hide();

            // hide the footer
            $('#pagefooter').hide();
        });
    </script>

    <style>
        .images {
            border: 4px solid #666;
            margin-bottom: 20px;
        }

    </style>

<div class="container-fluid">  
    <div class="row-fluid">
        <div class="span2"> </div>
        <div class="span8">

            <div class="images"><img src="lochbox_Word-edit1.png" /></div>
            <div class="images"><img src="lochbox_Word-edit2.png" /></div>
            <div class="images"><img src="lochbox_Word-edit3.png" /></div>
            <div class="images"><img src="lochbox_Word-edit4.png" /></div>
            <div class="images"><img src="lochbox_Word-edit5.png" /></div>
            <div class="images"><img src="lochbox_Word-edit6.png" /></div>
            <div class="images"><img src="lochbox_Word-edit7.png" /></div>

        </div>
    </div>
</div>

</asp:Content>

