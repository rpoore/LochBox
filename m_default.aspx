﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    string _relpath = null;
    string _sharedwithmePath = null;
    string _favoritesPath = null;
    string _breadcrumblabel = "LochBox";

    private void Page_Load(object sender, System.EventArgs e)
    {
        // verify that the user is logged in
        if (Session["sesUsername"] == null)
            Response.Redirect("/m_login.aspx?returnurl=" + Server.UrlEncode(Request.Url.ToString()), true);
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        // otherwise.....
        _favoritesPath = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_Favorites");
        _sharedwithmePath = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_SharedWithMe");
        
        // get the path and set the query for the table
        //   the path is relative to the parent of the user's home folder path on the server...e:\public\userhome\
        //   so _relpath is going to be either "username\path\to\stuff" or just "username"
        if (Request.QueryString["relpath"] != null)
        {
            // user selected path...
            _relpath = Server.UrlDecode(Request.QueryString["relpath"].ToString());
            _breadcrumblabel = _relpath.Substring(_relpath.LastIndexOf("\\") + 1);
            
            // make sure the text isn't too long
            if (_breadcrumblabel.Length > 11)
            {
                _breadcrumblabel = _breadcrumblabel.Substring(0, 10) + "...";
            }
        }
        else
        {
            // use the base path for the user's home folder
            _relpath = Session["sesUsername"].ToString();
        }

        // if this is a file, send it along....
        // write the file directly to the HTTP content output stream.
        if (Request.QueryString["type"] != null)
        {
            if (Request.QueryString["type"].ToString() == "F")
            {
                sendFiletoClient(Application["basePath"] + _relpath);
                Response.End();
            }
        }
    }

    private void sendFiletoClient(string strPath)
    {
        // get the file name from the path        
        string strFilename = strPath.Substring(strPath.LastIndexOf("\\") + 1);

        // content type
        if (strFilename.ToLower().EndsWith(".pdf"))
            Response.ContentType = "Application/pdf";
        else if (strFilename.ToLower().EndsWith(".jpg"))
            Response.ContentType = "Image/JPEG";
        else if (strFilename.ToLower().EndsWith(".gif"))
            Response.ContentType = "Image/GIF";
        else if (strFilename.ToLower().EndsWith(".html"))
            Response.ContentType = "text/HTML";
        else if (strFilename.ToLower().EndsWith(".htm"))
            Response.ContentType = "text/HTML";
        else if (strFilename.ToLower().EndsWith(".png"))
            Response.ContentType = "Image/PNG";
        else if (strFilename.ToLower().EndsWith(".bmp"))
            Response.ContentType = "Image/BMP";
        else if (strFilename.ToLower().EndsWith(".tiff"))
            Response.ContentType = "Image/TIFF";
        else if (strFilename.ToLower().EndsWith(".jpeg"))
            Response.ContentType = "Image/JPEG";
        else if (strFilename.ToLower().EndsWith(".txt"))
            Response.ContentType = "text/Plain";
        else if (strFilename.ToLower().EndsWith(".doc"))
            Response.ContentType = "application/msword";
        else
            Response.ContentType = "application/octet-stream";

        // write the file directly to the HTTP content output stream.
        Response.AddHeader("Content-Disposition", "inline; filename=\"" + strFilename + "\"");
        Response.WriteFile(strPath);
        Response.Flush();
        Response.End();
    }
 
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <title>CSU Lochbox Mobile</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
   <link rel="shortcut icon" href="/resources/ico/favicon.ico" />
   <link rel="apple-touch-icon" href="/resources/ico/favicon.ico" />

   <!-- style -->
   <link href="/resources/bootstrap/yeti/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="/resources/jtable.2.3.1/themes/metro/lightgray/jtable.min.css" rel="stylesheet" type="text/css" />
   <link href="resources/bootmetro/assets/css/bootmetro-icons.css" rel="stylesheet" />    

    <!-- override some styles rather than changing the sheets -->
    <style type="text/css">
        .jtable-data-row { height: 50px; }
        .navbar { background-color: #263a57; }
        .navbar-default .dropdown-menu { background-color: #263a57; }    
        .modal-header { background-color: #263a57; color: #eee;}  
        .jtable-column-header { display: none; color: #fff; }
        .alert-warning { background-color: #FCF8E3; color: #8A6D3B; font-weight: 500; border-color: #FAEBCC }
        .genlinkbackground { background-color: #eee; }

        /* override the default modal dialog fade.....don't like the slide too */
        .modal.fade .modal-dialog {
          -webkit-transform: translate(0, 0);
          -ms-transform: translate(0, 0);
          transform: translate(0, 0);
          -webkit-transition: -webkit-transform 0.3s ease-out;
          -moz-transition: -moz-transform 0.3s ease-out;
          -o-transition: -o-transform 0.3s ease-out;
          transition: transform 0.3s ease-out;
        }    
    </style>
 
    <!-- scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script> 
    <script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/jtable.2.3.1/jquery.jtable.min.js"></script>

    <script type="text/javascript">
        // global vars
        var $selectedFSOpath = null;
        var $selectedFSOtype = null;

        function fixedEncodeURIComponent(str) {
            return encodeURIComponent(str).replace(/[!'()*]/g, escape);
        }

        function updateSize() {
            $("#modalFileUpload").modal("show");

            var nBytes = 0,
                oFiles = document.getElementById("input").files,
                nFiles = oFiles.length;

            for (var nFileId = 0; nFileId < nFiles; nFileId++) {
                nBytes += oFiles[nFileId].size;
            }

            var sOutput = nBytes + " bytes";

            // optional code for multiples approximation
            for (var aMultiples = ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
                sOutput = nApprox.toFixed(0) + " " + aMultiples[nMultiple];
            }
            // end of optional code

            document.getElementById("fileNum").innerHTML = nFiles;
            document.getElementById("fileSize").innerHTML = sOutput;
        }

        $(document).ready(function () {            
            var dataitem;          

            $('#fsobjecttable').jtable({
                selecting: true,
                actions: {
                    listAction: '/handlers/fsobjects.ashx?q=list&u=<%=Session["sesUsername"].ToString() %>&relpath=<%=Server.UrlEncode(_relpath) %>',
                },
                fields: {
                    NAME: {
                        title: '/',
                        display: function (data) {
                            // get the file name
                            var recordname = data.record.NAME;

                            // size the text depending on the window size.
                            var browidth = $(window).width();
                            var txtlength = browidth / 13 ;
                                  
                            if (recordname.length >= txtlength)
                                recordname = recordname.substring(0, txtlength) + "...";

                            var filename = data.record.NAME;

                            var returnstring = "<i class='" + data.record.ICON + "' style='font-size: 2.3em; color: orange; vertical-align: middle;'></i>&nbsp;&nbsp;&nbsp;&nbsp;" +
                                    "<a href='/m_default.aspx?type=" + data.record.TYPE + "&relpath=" + data.record.PATH + "' style='font-size: 1.3em; vertical-align: middle;' >" +
                                    recordname +
                                    "</a>";

                            returnstring += "<span class='pull-right'>" +
                                    "<a href='#' class='fsoinfo' data-path='" + data.record.PATH + "' data-filename='" + filename + "' data-type='" + data.record.TYPE + "' style='text-decoration: none;'><i class='icon-info-4' style='color: #aaa; font-size: 1.9em; margin-left: 0px; padding-right: 5px; vertical-align: middle;'></i></a>  " +
                                    "</span>";

                            return returnstring;
                        }
                    }
                },
                recordsLoaded: function (data) {                    
                    $('.fsoinfo').on('click', function (e) {
                        e.stopPropagation();
                        var $this = $(this);
                       
                        // set the currently selected object label
                        $(".selectedObjectName").text($this.attr("data-filename"));

                        // set the path and object type
                        $selectedFSOpath = $this.attr("data-path");
                        $selectedFSOtype = $this.attr("data-type");

                        // is this a directory?  if so, hide some actions
                        if ($selectedFSOtype == "D") {
                            $("#addToFavoritesAction").show();
                            $("#shareAction").show();
                            $("#linkAction").hide();
                        }

                        // if this is a file, hide other things
                        if ($selectedFSOtype == "F") {
                            $("#addToFavoritesAction").hide();
                            $("#shareAction").hide();
                            $("#linkAction").show();
                        }

                        $('#actionLinks').show();
                        $('#modalDetails').modal('show');                        
                    });

                    // set the public link modal
                    $('#modalPublicLink').on('shown', function () {
                        // get the public url                            
                        $("#modalPublicLinkNewLink").load("/handlers/fsobjects.ashx?q=getpubliclink&relpath=" + $selectedFSOpath, { u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });
                    });

                    return false;
                }
            });

            $('#fsobjecttable').jtable('load', { u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });            

            $('#modalNewfolder').on('hide', function () { $('#modalNewfolderName').val(''); $('.modalResults').text(''); });

            $('#submitNewfolderName').click(function () {
                // try to create the folder and show the results
                if ($('#modalNewfolderName').val() != '') {
                    $postdata = $.post("/handlers/fsobjects.ashx?q=createobject", { selectedObjectType: 'D', nfname: fixedEncodeURIComponent($('#modalNewfolderName').val()), relpath: '<%=Server.UrlEncode(_relpath)%>', u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });

                    $postdata.done(function ($returnvalue) {
                        if ($returnvalue === "Directory created!") {
                            $('#modalNewfolder').modal('hide');                            
                            $('#fsobjecttable').jtable('reload');                           
                        }
                        else {
                            $('.modalResults').text($returnvalue);
                        }
                    });
                }

                //return false;
            });
            // ------------------------------------------------------------------------------ //

            // add to favorites action link clicked            
            $('#addToFavoritesAction').click(function () {
                $('#detailsCloseButton').hide();
                $('#actionLinks').hide();
                $('#alertAddToFavorites').show();
                $('.modalResults').load("/handlers/addtofavorites.ashx", { relpath: $selectedFSOpath, u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });                
            });
            // ------------------------------------------------------------------------------ //          

            // delete action link clicked
            $('#deleteAction').click(function () {
                $('#detailsCloseButton').hide();
                $('#actionLinks').hide();
                $('#alertDelete').show();
            });

            // delete button clicked
            $('#deleteButton').click(function () {
                $postdata = $.post("/handlers/fsobjects.ashx?q=deleteobject", { selectedObjectType: $selectedFSOtype, relpath: $selectedFSOpath, u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });

                $postdata.done(function (returnvalue) {
                    $('#alertDelete').hide();
                    $('#modalDetails').modal('hide');
                    $('#fsobjecttable').jtable('reload');
                });                
            });
            // ------------------------------------------------------------------------------ //

            // link action link clicked
            $('#linkAction').click(function () {
                $('#detailsCloseButton').hide();
                $('#actionLinks').hide();
                $('#alertPublicLink').show();

                $postdata = $.post("/handlers/fsobjects.ashx?q=getpubliclink&relpath=" + $selectedFSOpath, { u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });

                $postdata.done(function (returnvalue) {
                    // if it's a link that's returned, show the e-mail function
                    if (returnvalue.indexOf("https://lochbox.clayton.edu") == 0) {
                        $('.modalResults').html('<a href="' + returnvalue + '" style="color: blue;">Link</a> created.');
                        $('#emailLink').show();
                        $('#emailLinkHREF').attr('href', 'mailto:Someone?subject=<%=Session["sesUsername"].ToString() %> shared a link with you!&body=' + returnvalue + '.');
                    }
                    else {
                        // otheriwse, return something else.
                        $('.modalResults').text(returnvalue);
                    }
                });                
            });
            // ------------------------------------------------------------------------------ //

            // rename action link clicked
            $('#renameAction').click(function () {
                $('#detailsCloseButton').hide();
                $('#alertRename').show();
                // auto-focus here is a bit anoying on the phone.
                //$('#renameText').focus();
                $('#actionLinks').hide();
            });
            // ------------------------------------------------------------------------------ //

            // share action link clicked
            $('#shareAction').click(function () {
                $('#alertShare').show();
                $('#detailsCloseButton').hide();
                $('#actionLinks').hide();

                // get list of current users
                $postdata = $.post("/handlers/fsobjects.ashx?q=currentusers&selectedObjectType=" + $selectedFSOtype +
                    "&relpath=" + $selectedFSOpath, { u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' }); 

                $postdata.done(function (returnvalue) {
                    if (returnvalue != "") {
                        $("#modalShareCurrentUsers").html("<strong>Current permissions:</strong><br />" + returnvalue);
                    }
                    else {
                        $("#modalShareCurrentUsers").empty();
                    }
                });
            });
            // ------------------------------------------------------------------------------ //

            // the file upload link clicked
            $("#fileSelect").click(function (e) {
                e.preventDefault();
                $("#input").click();
                $('.modalResults').empty();
                $('#detailsCloseButton').hide();
                $("#continueuploadbutton").show();
            });
            // ------------------------------------------------------------------------------ //

            // continue upload buton clicked
            $("#continueuploadbutton").on('click', function () {
                var formData = new FormData($('#form1')[0]);
                
                $.ajax({
                    type: 'POST',
                    url: '/handlers/fsobjects.ashx?q=upload&relpath=<%=Server.UrlEncode(_relpath)%>',
                    data: formData,
                    async: true,
                    contentType: false,
                    processData: false,
                    cache: false,
                    beforeSend: function () {
                        // hide that button
                        $("#continueuploadbutton").hide();
                        $('#modalUploadCloseButton').hide();
                        $('.modalResults').html("<br /><i>Uploading</i>  <img src='resources/img/imgloader.gif' style='padding-left: 10px;'/>");                    
                    },
                    success: function (returnvalue) {
                        //$("#modalFileUpload").children().remove();
                        //$("#modalFileUpload").modal("hide");                        
                        $('.modalResults').html("<br />Upload complete!");
                        $('#modalUploadCloseButton').show();
                        $('#fsobjecttable').jtable('reload');                        
                    }
                });
            });

            $("#modalUploadCloseButton").on('click', function () {
                $('.modalResults').empty();
            });
            // ------------------------------------------------------------------------------ //

            // close the parent div for cancel clicks
            $('.cancel').click(function (e) {
                $('.modalResults').empty();
                $('#renameText').empty();
                $('#actionLinks').show();
                $('#detailsCloseButton').show();
                $(this).parent().hide();
            });
            // ------------------------------------------------------------------------------ //

        });
    </script>

</head>
<body style="margin: 0px; padding: 5px; padding-top: 65px;">
    <form id="form1" runat="server" enctype="multipart/form-data">

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <span class="navbar-brand"><%=_breadcrumblabel %></span>

            <div class="dropdown pull-right"> 
                <%--<a href="/m" class="navbar-brand" style="font-size: 22px; color: #eee; text-decoration: none;"><b class="icon-home-2"></b></a>--%>
                <a href="m?type=D&relpath=<%=_favoritesPath %>" class="navbar-brand" style="font-size: 22px; color: #eee; text-decoration: none;"><b class="icon-bookmark-3"></b></a>
                <a href="m?type=D&relpath=<%=_sharedwithmePath %>" class="navbar-brand" style="font-size: 22px; color: #eee; text-decoration: none;"><b class="icon-users-2"></b></a>             
                <a href="#" class="dropdown-toggle navbar-brand" data-toggle="dropdown" style="font-size: 22px; color: #eee; text-decoration: none;"><b class="icon-plus-2"></b></a>
                <ul class="dropdown-menu" style="margin-top: 20px;">
                    <li><a href="#modalNewfolder" data-toggle="modal" id="newfolder" title="New Folder" style="font-size: 16px;"><i class="icon-folder" style="padding-right: 7px; font-size: 22px;"></i>New folder</a></li>                     
                    <li><a href="#" id="fileSelect" title="Upload files" style="font-size: 16px;"><i class="icon-upload" style="padding-right: 7px; font-size: 22px;"></i>Upload a file</a></li> 
                </ul>
                <!-- the ugly file input button thingy -->  
                <input type="hidden" id="u" value="<%=Session["sesUsername"].ToString() %>" />
                <input type="hidden" id="sesID" value="<%=Session["sesID"].ToString() %>" />        
                <input type="file" id="input" name="input" onchange="updateSize();" accept="*" style="display: none;" /> 
            </div> 

        </div><!-- /.navbar-header -->
      </div><!-- /.container-fluid -->
    </nav>
        
    <div>        
        <div id="fsobjecttable" style="margin-top: -10px;"></div>
    </div>

    <!-- Modal dialogs -->
    <!-- Public Link modal -->
    <div id="modalPublicLink" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <h4 id="modalPublicLinkLabel">Public Link</h4>
        </div>
        <div class="modal-body">
            <p><span id="modalPublicLinkNewLink"></span></p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>          
        </div>
    </div>

    <!-- Details modal -->
    <div class="modal fade" id="modalDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">            
            <h4 class="modal-title">Details</h4>
          </div>
          <div class="modal-body">
            <p><span class="selectedObjectName" style="font-weight: bold; text-decoration: underline;"></span></p>

            <!-- Add to favorites thingy -->
            <div id="alertAddToFavorites" style="display: none;">                
                <span class="modalResults"></span>
                <br /><br />
                <button type="button" class="btn btn-sm btn-default cancel">Close</button>
            </div>
            
            <!-- Delete thingy -->
            <div id="alertDelete" style="display: none;">
                <b class="icon-warning-2" style="font-size: 1.5em; padding-right: 5px;"></b>
                Delete this item?
                <br /><br />
                <button type="button" class="btn btn-sm btn-default cancel">Cancel</button>
                <button type="button" class="btn btn-sm btn-danger" id="deleteButton">Delete</button>                
                <span class="modalResults"></span>
            </div>

            <!-- Public link thingy -->
            <div id="alertPublicLink" style="display: none;">                
                <span class="modalResults"></span>
                <br /><br />
                <div id="emailLink" style="display: none;">
                    <a href="#" id="emailLinkHREF" style="color: blue;">E-mail</a> the link to someone.
                    <br /><br />
                </div>
                <button type="button" class="btn btn-sm btn-default cancel">Close</button>
            </div>

            <!-- Rename thingy -->
            <div id="alertRename" style="display: none;">
                <input type="text" id="renameText" placeholder="Enter new name..." style="width: 210px;" />
                <br /><br />
                <button type="button" class="btn btn-sm btn-default cancel">Cancel</button>
                <button type="button" class="btn btn-sm btn-primary" id="renameButton">Save Changes</button>                
                <span class="modalResults"></span>
            </div>

            <!-- Share thingy -->
            <div id="alertShare" style="display: none;">                
                <p><span id="modalShareCurrentUsers"></span></p> 
                <br />
                Desired access level for new user:
                <select class="span2 input-small" id="optionsAccessLevel">
                    <option value="R">Read Only</option>
                    <option value="C">Read and Write</option>
                </select>                 
                <br /><br />
                <input type="text" placeholder="Enter valid CSU account..." class="form-control" id="modalShareNewShareUser" />                                         
                <br /><br />                                
                <button type="button" class="btn btn-sm btn-default cancel">Cancel</button>  
                <button class="btn btn-sm btn-primary" type="button" id="modalShareAddUserButton">Add User</button>
                <span class="modalResults"></span>
            </div>
            
            <div id="actionLinks">
                <ul class="nav nav-pills nav-stacked" style="font-size: 1.1em;">
                    <li class="genlinkbackground"><a href="#" id="addToFavoritesAction"><i class="icon-star" style="padding-right: 5px;"></i>Add to Favorites</a></li>
                    <li class="genlinkbackground"><a href="#" id="deleteAction"><i class="icon-remove-2" style="padding-right: 5px;"></i>Delete</a></li>
                    <li class="genlinkbackground"><a href="#" id="linkAction"><i class="icon-link" style="padding-right: 5px;"></i>Link</a></li>                    
                    <li class="genlinkbackground"><a href="#" id="renameAction"><i class="icon-type" style="padding-right: 5px;"></i>Rename</a></li>
                    <li class="genlinkbackground"><a href="#" id="shareAction"><i class="icon-share" style="padding-right: 5px;"></i>Share</a></li>            
                </ul>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" id="detailsCloseButton">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- New folder modal -->
    <div class="modal fade" id="modalNewfolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">            
            <h4 class="modal-title">New Folder</h4>
          </div>
          <div class="modal-body">
                <p><input type="text" id="modalNewfolderName" placeholder="Enter folder name..." style="width: 250px;"/></p>
                <p><span class="modalResults"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary btn-sm" id="submitNewfolderName">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Upload file modal -->
    <div class="modal fade" id="modalFileUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">            
            <h4 class="modal-title">File Upload</h4>
          </div>
          <div class="modal-body">
            <span id="fileNum">0</span> file(s)<br />
            Total size: <span id="fileSize">0</span><br />
            <button type="button" class="btn btn-primary btn-sm" id="continueuploadbutton">Upload</button>
            <p><span class="modalResults"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" id="modalUploadCloseButton">Close</button>            
          </div>
        </div>
      </div>
    </div>

    </form>
</body>
</html>
