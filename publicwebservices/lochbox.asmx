﻿<%@ WebService Language="C#" Class="lochbox" %>

using System;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.DirectoryServices;
using MySql.Data.MySqlClient;
using System.Web.Configuration;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class lochbox  : System.Web.Services.WebService {
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public string AuthUser(string username, string password)
    {
        string returnValue = "AUTH_FAILED";

        try
        {
            DirectoryEntry entry = new DirectoryEntry("LDAP://", username, password);
            object nativeobject = entry.NativeObject;
            returnValue = "AUTH_SUCCESS";
        }
        catch (DirectoryServicesCOMException)  {  }
        
        return string.Format("{{ \"AuthResponse\" : \"{0}\" }}", returnValue);        
    }   
    

    [WebMethod]
    public string logClient(string username, string netinfo)
    {
        string returnValue = null;
        
        // try to log the client request
        try
        {
            MySqlConnection con = new MySqlConnection(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);

            string sql = "insert into accesslog (function,data,response,username,remote_host) " +
                "values ('Login:WebDAV','', 'SUCCESS', '" + username + "','" + netinfo + "')";

            MySqlCommand cmd = new MySqlCommand(sql, con);
            con.Open();
            cmd.ExecuteNonQuery();

            con.Close();

            returnValue = "OK";
        }
        catch (Exception exp) { returnValue = "FAILED_LOGGING"; }

        return returnValue;
    }
    
}