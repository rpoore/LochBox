﻿<%@ WebService Language="C#" Class="url" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using MySql.Data.MySqlClient;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class url  : System.Web.Services.WebService {
        
    [WebMethod(Description = "Return short URI in plain text format.")]
    public void getShortURL(string strOriginalURL)
    {
        string returnValue = null;        
        string strShortURL = "/L0k" + RandomString(12).ToString();

        LochBoxHelpers lochy = new LochBoxHelpers();
        MySqlConnection con = lochy.createDBConnection();
        
        string sql = "insert into urllookup (original_url,short_url) values (?original_url,?short_url)";
        MySqlCommand cmd = new MySqlCommand(sql, con);

        cmd.Parameters.Add("?original_url", strOriginalURL);
        cmd.Parameters.Add("?short_url", strShortURL);
        
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
        
        returnValue = "https://lochbox.clayton.edu" + strShortURL;
        
        sendPlainTextResponse(returnValue);
    }

    // generate a random string for the request.path encoding...
    private string RandomString(int size)
    {
        Random rng = new Random();
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";        
        
        char[] buffer = new char[size];

        for (int i = 0; i < size; i++)
        {
            buffer[i] = chars[rng.Next(chars.Length)];
        }
        
        return new string(buffer);
    }

    // send the value to the client
    private void sendPlainTextResponse(string value)
    {
        HttpContext.Current.Response.ContentType = "text/plain";
        HttpContext.Current.Response.Write(value);
    }  
    
    // check for the existence of shorturl in the database
    // -----NOT USED-------
    private bool shortURLExists(string shorturl)
    {
        bool blnReturnValue = false;
        LochBoxHelpers lochy = new LochBoxHelpers();    
        string sql = "select short_url from  urllookup where short_url = '" + shorturl + "'";
        MySqlConnection con = lochy.createDBConnection();
        con.Open();
        MySqlCommand cmd = new MySqlCommand(sql, con);
        string result = cmd.ExecuteScalar().ToString();
        con.Close();

        if (result != null)
        {
            blnReturnValue = true;
        }
        
        return blnReturnValue;
    }    
}