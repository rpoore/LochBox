﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
    string _relpath = null;
    string _publicPath = null;
    string _sharedwithmePath = null;
    string _favoritesPath = null;
    string _pixGallery = null;
    string _notesPath = null;
            
    private void Page_Load(object sender, System.EventArgs e)
    {
        // verify that the user is logged in
        if (Session["sesUsername"] == null)            
            Response.Redirect("/login?returnurl=" + Server.UrlEncode(Request.Url.ToString()), true);        
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        // otherwise.....
       
        // set the path for the user's SharedWithMe folder
        _publicPath = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_Public");
        _sharedwithmePath = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_SharedWithMe");
        _favoritesPath = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_Favorites");
        _pixGallery = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_Public\\Gallery\\albums");
        _notesPath = Server.UrlEncode(Session["sesUsername"].ToString() + "\\" + "_Notes");
        
        // get the path and set the query for the table
        //   the path is relative to the parent of the user's home folder path on the server...e:\public\userhome\
        //   so _relpath is going to be either "username\path\to\stuff" or just "username"
        if (Request.QueryString["relpath"] != null)
        {
            // user selected path...
            _relpath = Server.UrlDecode(Request.QueryString["relpath"].ToString());
            
            // form the breadcrumbs
            string parentPath = _relpath.Substring(0, _relpath.LastIndexOf("\\"));
            string[] arrPath = _relpath.Split('\\');
            int arrPathLength = arrPath.Length;
            int index = 0;
            
            foreach (string fol in arrPath)
            {
                // don't show username
                if (fol != Session["sesUsername"].ToString())
                {
                    // let's make the last path index not a hyperlink
                    if (index == arrPathLength-2)
                        locBreadcrumbs.Text += "<li>&nbsp;/&nbsp;" + fol + "</li>";
                    else
                        locBreadcrumbs.Text += "<li>&nbsp;/&nbsp;<a href=\"home?type=D&relpath=" + Server.UrlEncode(parentPath) + "\">" + fol + "</a></li>";
                   
                    index++;
                }
            }            
        }
        else
        {
            // use the base path for the user's home folder
            _relpath = Session["sesUsername"].ToString();            
            locBreadcrumbs.Text = "";
        }
        
        // if this is a file, send it along....
        // write the file directly to the HTTP content output stream.
        if (Request.QueryString["type"] != null)
        {
            if (Request.QueryString["type"].ToString() == "F")             
            {
                sendFiletoClient(Application["basePath"] + _relpath);
                Response.End();
            }
        }        
    }

    private void sendFiletoClient(string strPath)
    {
        // get the file name from the path        
        string strFilename = strPath.Substring(strPath.LastIndexOf("\\") + 1);
        
        // content type
        if (strFilename.ToLower().EndsWith(".pdf"))
            Response.ContentType = "Application/pdf";
        else if (strFilename.ToLower().EndsWith(".jpg"))
            Response.ContentType = "Image/JPEG";
        else if (strFilename.ToLower().EndsWith(".gif"))
            Response.ContentType = "Image/GIF";
        else if (strFilename.ToLower().EndsWith(".html"))
            Response.ContentType = "text/HTML";
        else if (strFilename.ToLower().EndsWith(".htm"))
            Response.ContentType = "text/HTML";
        else if (strFilename.ToLower().EndsWith(".png"))
            Response.ContentType = "Image/PNG";
        else if (strFilename.ToLower().EndsWith(".bmp"))
            Response.ContentType = "Image/BMP";
        else if (strFilename.ToLower().EndsWith(".tiff"))
            Response.ContentType = "Image/TIFF";
        else if (strFilename.ToLower().EndsWith(".jpeg"))
            Response.ContentType = "Image/JPEG";
        else if (strFilename.ToLower().EndsWith(".txt"))
            Response.ContentType = "text/Plain";
        else if (strFilename.ToLower().EndsWith(".doc"))
            Response.ContentType = "application/msword";
        else
            Response.ContentType = "application/octet-stream";
                
        // write the file directly to the HTTP content output stream.
        Response.AddHeader("Content-Disposition", "inline; filename=\"" + strFilename + "\"");
        Response.WriteFile(strPath);
        Response.Flush();
        Response.End();
    }

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        // global vars
        var $selectedFSOpath = null;
        var $selectedFSOtype = null;
        var $editorcreatefile = null;
        
        function playTunes(songpath, songname) {
            // show the player
            $('#audioPlayerdiv').show();

            // show the now playing text
            $('#nowplayingText').text(songname);

            var sourceUrl = songpath;
            var audio = $("#audioPlayer");      
          
            $("#mp3src").attr("src", sourceUrl).appendTo(audioPlayer);

            audio[0].play();
        }

        function fixedEncodeURIComponent (str) {
          return encodeURIComponent(str).replace(/[!'()*]/g, escape);
        }

        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        function checkSessionEnd() {
            $.ajax({
                type: 'POST',
                url: '/handlers/sessionHandler.ashx',
                success: function (returnvalue) {
                    //if (returnvalue == "SESSION_INVALID")
                        //$(location).attr('href', 'https://lochbox.clayton.edu');
                }
            });
        }

        $(document).ready(function () {
            // used to store post data results
            var $postdata = null;

            // check for session timeout
            //setInterval(function () { checkSessionEnd() }, 30000);

            // set the username labels
            $('.usernamelabel').text('<%=Session["sesUsername"].ToString() %>');            

            // is this coming from LochBoxShare?
            if (getUrlVars()["action"] != null) {
                var $pageaction = getUrlVars()["action"];
               
                if ($pageaction == "share") {
                    $('#modalShareFolderName').text(decodeURIComponent(getUrlVars()["folname"]));
                    $('#modalShare').modal('show');
                }
            }

            $('#fsobjecttable').jtable({
                title: '',
                selecting: true,
                multiselect: false,
                selectingCheckboxes: false,
                sorting: false,
                defaultSorting: 'Name ASC',
                actions: {
                    listAction: '/handlers/fsobjects.ashx?q=list&relpath=<%=Server.UrlEncode(_relpath) %>',
                },
                fields: {
                    NAME: {
                        title: 'Name',
                        width: '70%',   
                        display: function (data) {
                            // show certain things based on the item type
                            // probably a better way to do this

                            // get the file name
                            var filename = data.record.NAME.toLowerCase();

                            var returnstring = "<div style='height: 3px;'></div><i class='" + data.record.ICON + "' style='font-size: 22px; color: #f58426; padding-right: 16px; vertical-align: middle;'></i>";
                            
                            returnstring += "<a href='./home?type=" + data.record.TYPE + "&relpath=" + data.record.PATH + "'>" +
                                    data.record.NAME +
                                    "</a>"; 
  
                            returnstring += "<span class='dropdown editrow' style='margin-left: 10px;'><a class='header-dropdown dropdown-toggle accent-color' data-toggle='dropdown' href='#' style='text-decoration: none;'>" +
                                    "<i class='icon-arrow-down-6' style='color: gray'></i></a>" +
                                    "<ul class='dropdown-menu'>";

                            // allow Play link for mp3's        
                            if (filename.indexOf('.mp3') != -1)
                                returnstring += "<li><a href='#audioPlayerdiv' onclick='playTunes(\"" + data.record.MUSICPATH + "\",\"" + filename + "\"); return false;'>Play</a></li><li class='divider'></li>";
                                                                    
                            returnstring += "<li><a href='#modalDelete' data-toggle='modal'>Delete</a></li>" +
                                         "<li><a href='#modalRename' data-toggle='modal'>Rename</a></li>";

                            if (filename.indexOf('.txt') != -1 || filename.indexOf('.htm') != -1 || filename.indexOf('.html') != -1 || filename.indexOf('.cmd') != -1 || filename.indexOf('.ps1') != -1 || filename.indexOf('.ini') != -1 || filename.indexOf('.css') != -1)
                                returnstring += "<li class='divider'></li><li><a href='#modalEdit' class='editfile' data-toggle='modal'>Edit</a></li>";

                            //if (filename.indexOf('.doc') != -1 || filename.indexOf('.docx') != -1 || filename.indexOf('.xls') != -1 || filename.indexOf('.xlsx') != -1 || filename.indexOf('.ppt') != -1 || filename.indexOf('.pptx') != -1 || filename.indexOf('.pptx') != -1)
                            //    returnstring += "<li class='divider'></li><li><a href='https://newton.clayton.edu/op/view.aspx?src=' target='_blank'>Quick View</a></li>";

                            // allow Get Public Link for files under _Public
                            if (data.record.TYPE == 'F' && data.record.PATH.indexOf('_Public') != -1 )
                                returnstring += "<li class='divider'></li><li><a href='#modalPublicLink' data-toggle='modal'>Get Public Link</a></li>";

                            // allow the Share link but only on directories and not under _SharedWithMe
                            if (data.record.TYPE == 'D' && data.record.PATH.indexOf('_SharedWithMe') == -1)
                                returnstring += "<li class='divider'></li><li><a href='#modalShare' data-toggle='modal'>Share</a></li>";
                                
                            // allow Add to Favorites for folders
                            if (data.record.TYPE == 'D')
                                returnstring += "<li class='divider'></li>" +
                                    "<li><a href='#modalAddToFaves' data-toggle='modal'>Add to favorites</a></li>";

                            // end the returnstring
                            returnstring += "</ul>" + "</span><div style='height: 3px;'></div>";
                            
                            return returnstring;
                        }
                    },
                    SIZE: {
                        title: 'Size',
                        width: '15%'
                    },
                    LASTMODIFIED: {
                        title: 'Last Modified',
                        width: '15%'
                    },
                    PATH: {
                        title: 'Path',                        
                        list: false
                    }
                },
                selectionChanged: function () {
                    // get the fs object that was selected
                    var $selectedRows = $('#fsobjecttable').jtable('selectedRows');

                    if ($selectedRows.length > 0) {
                        //Show selected rows
                        $selectedRows.each(function () {
                            var record = $(this).data('record');

                            // set the values for things.....probably a better way to do this
                            $('#modalRenameCurrentName').text(record.NAME);
                            $('#modalDeleteObjName').text(record.NAME);
                            $('#modalShareFolderName').text(record.NAME); 
                            $selectedFSOpath = record.PATH;
                            $selectedFSOtype = record.TYPE;                                                                                                                                                                                                    
                        });                            
                    }
                },
                recordsLoaded: function (data) { 
                    // after all the records are loaded and the dust has settled...
                }
            });

            // load the main table
            $('#fsobjecttable').jtable('load', {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' }); 
         
            // now, handle some events
            // ------------------------------------------------------------------------------ //
            // new folder
            $('#modalNewFolder').on('shown', function() { $('#modalNewFolderName').focus(); });

            $('#submitNewFolderName').click(function () 
            {
                // try to create the folder and show the results
                if ($('#modalNewFolderName').val() !== '') 
                {
                    $postdata = $.post("/handlers/fsobjects.ashx?q=createobject", { selectedObjectType: 'D', nfname: fixedEncodeURIComponent($('#modalNewFolderName').val()), relpath: '<%=Server.UrlEncode(_relpath)%>', u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });

                    $postdata.done(function($returnvalue) 
                    {                        
                        if ($returnvalue === "Directory created!")
                        {
                            $('#modalNewFolder').modal('hide');
                            $('#fsobjecttable').jtable('reload'); 
                        }
                        else
                        {
                            $('#modalNewFolderResults').text($returnvalue);
                        }                       
                    });                    
                } 

                return false;
            });
            // ------------------------------------------------------------------------------ //

            // delete folder or file
            $('#modalDeleteButton').click(function () {
               $postdata = $.post("/handlers/fsobjects.ashx?q=deleteobject", { selectedObjectType: $selectedFSOtype, relpath: $selectedFSOpath, u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });

               $postdata.done(function (returnvalue) {
                    $('#modalDelete').modal('hide');
                    $('#fsobjecttable').jtable('reload');
                });

                return false;
            });
            // ------------------------------------------------------------------------------ //

            // rename folder or file
            $('#modalRenameButton').click(function () {
                if ($('#modalRenameNewName').val() !== '') {
                    $("#modalRenameResults").load("/handlers/fsobjects.ashx?q=renobject&nfname=" + fixedEncodeURIComponent($('#modalRenameNewName').val()) + "&selectedObjectType=" + $selectedFSOtype + 
                        "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' }); 

                    // hide the delete modal
                    $('#modalRename').modal('hide')

                    // reload the table
                    $('#fsobjecttable').jtable('reload');  
                }
                return false;
            });
            // ------------------------------------------------------------------------------ //

            // edit existing file 
            $('#modalEdit').on('shown', function () {
                // clear any previous results
                $('#modalEditSaveResults').text('');

                // let's make this dialog draggable
                $("#modalEdit").draggable(); 

                // get the file contents
                $postdata = $.post("/handlers/fsobjects.ashx?q=openfile&selectedObjectType=" + $selectedFSOtype + 
                    "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });  

                $postdata.done(function(fileContents) {      
                    // set the value of the textarea
                    $('#textFileContents').val(fileContents);

                    // init the editor
                    $("#textFileContents").cleditor({
                        width:        '100%', 
                        height:       400, 
                        controls:     "bold italic underline strikethrough subscript superscript | font size " +
                                    "style | color highlight removeformat | bullets numbering | outdent " +
                                    "indent | alignleft center alignright justify | undo redo | " +
                                    "rule image link unlink | cut copy paste pastetext | source",   
                       })[0].updateFrame();
                }); 
            });

            // save edited file
            $('#modalEditSave').on('click', function () {
                // save contents to a file
                $postdata = $.post("/handlers/fsobjects.ashx?q=savefile", { relpath: $selectedFSOpath, u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', v: $('#textFileContents').val() });
                
                $postdata.done(function(result) {      
                    $('#modalEditSaveResults').text(result);                     
                }); 
            });

            // create new file
            $('#modalNewFile').on('shown', function () {
                $('#newFileName').focus();

                // save button pressed
                $('#btnNewFileNameSave').on('click', function (e) 
                {
                    if ($('#newFileName').val() !== '') 
                    {
                        //var filename = $('#newFileName').val();

                        // let's save this file and open it in the editor
                        $postdata = $.post("/handlers/fsobjects.ashx?q=createobject", { selectedObjectType: 'F', nfname: fixedEncodeURIComponent($('#newFileName').val()), relpath: '<%=Server.UrlEncode(_relpath)%>', u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });


                        //$postdata = $.post("/handlers/fsobjects.ashx?q=createfile&filename=" + fixedEncodeURIComponent(filename) + "&relpath=<%=Server.UrlEncode(_relpath)%>", {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });

                        $postdata.done(function($returnvalue) {                               
                            if ($returnvalue == "File created.")
                            {   
                                // if we created the file, open it with the edit modal
                                 $('#modalNewFile').modal('hide');

                                // reload the file object table
                                $('#fsobjecttable').jtable('reload');  

                                 // work on this stuff later.....
                                 // add the file name to the path
                                 //$selectedFSOpath += "\\" + filename;
                                 //$('#modalEdit').modal('show');                                
                            }
                            else
                            {                          
                                $('#createNewFileResults').text($returnvalue);
                            }
                        });
                    }

                    return false;
                });

            });
            // ------------------------------------------------------------------------------ //

            // add to faves 
            $('#modalAddToFaves').on('shown', function () {
                $('#modalAddToFaveResults').load("/handlers/addtofavorites.ashx", { relpath: $selectedFSOpath, u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' });                  
                return false;
            });
            // ------------------------------------------------------------------------------ //

            // ------------------------------------------------------------------------------ //
            // public link modal
            $('#modalPublicLink').on('shown', function () {
                // get the public url 
                $("#modalPublicLinkNewLink").load("/handlers/fsobjects.ashx?q=getpubliclink&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' }, function() {
                    // clear the inputs
                    $('#PublicLinkEmailAddys').val('');
                    $('#PublicLinkEmailText').val('');

                    // set the text for the email information
                    $('#PublicLinkEmailText').val("<%=Session["sesUsername"].ToString() %> has shared the following link with you:\r\r" + $("#modalPublicLinkNewLink").text());

                    // handle the message being sent
                    $('#PublicLinkSendEmail').on('click', function() {
                        $.post("/handlers/fsobjects.ashx?q=sendpublink", {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', v: $('#PublicLinkEmailText').val(), to: $('#PublicLinkEmailAddys').val()} );    
                    });
                });                   
               
                return false;
            });
            // ------------------------------------------------------------------------------ //

            // ------------------------------------------------------------------------------ //
            // leave feedback
            $('#btnLeaveFeedback').on('click', function () {
                $('#modalFeedback').modal('show');

                return false;
            });

            $('#submitFeedback').click(function() {
                if ($('#feedback').val() !== '') {

                    $postdata = $.post("/handlers/leavefeedback.ashx", { from: '<%=Session["sesEmail"].ToString() %>', v: $('#feedback').val(), sesID: '<%=Session["sesID"].ToString() %>' });

                    $postdata.done(function (result) {
                        $('#modalFeedbackResults').text(result);

                        // disallow further feedback
                        $('#submitFeedback').hide();
                        $('#feedback').attr('disabled', 'true');
                    });
                }

                return false;
            });
            // ------------------------------------------------------------------------------ //

            // ------------------------------------------------------------------------------ //
            // share folder
            $('#modalShare').on('shown', function () {
                // the autocomplete user search
                $('#modalShareNewShareUser').autocomplete({
                    source: "/handlers/userSearch.ashx",
                    minLength: 3,
                });

                // show the results                               
                $("#modalShareCurrentUsers").load("/handlers/fsobjects.ashx?q=currentusers&selectedObjectType=" + $selectedFSOtype + 
                    "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' }); 
            });

            $('#modalShare').on('hide', function () {
                $('.ui-helper-hidden-accessible').empty();
            });

            // remove user clicked
            $('#modalShare').on('click', '.removeuser', function() {
                var $this = $(this);
                var $user = $this.attr("data-user");

                // now try to remove the user
                $.post("/handlers/fsobjects.ashx?q=removeuser&accesslevel=Z&otheruser=" + $user + 
                  "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>', remotehost: '<%=Request.ServerVariables["REMOTE_HOST"] %>' }); 
                        
                // reload the data                             
                $("#modalShareCurrentUsers").load("/handlers/fsobjects.ashx?q=currentusers&selectedObjectType=" + $selectedFSOtype + 
                    "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });
                     
                return false;
            });

            // file upload modal    
            $('#modalFileUp').on('shown', function () {
                //$('#modalFileUp').draggable();
            });

            // refresh the table if closed
            $('#modalFileUpCloseButton').click(function () { $('#fsobjecttable').jtable('reload');  })

            // share folder - add user
            $('#modalShareAddUserButton').click(function () 
            {
                // try to add the user
                if ($('#modalShareNewShareUser').val() != "")
                {
                    //$('#modalShareAddUserButton').text('Adding User...');
                    //$('#adduserwaitspinner').show();

                    //$("#modalShareAddUserResults").load
                    $postdata = $.post("/handlers/fsobjects.ashx?q=adduser&accesslevel=" + $('#optionsAccessLevel').val() + "&usertoadd=" + $('#modalShareNewShareUser').val() + 
                        "&selectedObjectType=" + $selectedFSOtype + "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' }, function () { }); 
                    
                    $postdata.done(function($returnvalue) 
                    {
                        //$('#modalShareAddUserButton').text('Add User');
                        //$('#adduserwaitspinner').hide();

                        if ($returnvalue !== "Permissions applied!")
                        {
                            $('#modalShareAddUserResults').html($returnvalue);    
                        }

                        // reload the modal
                        $("#modalShareCurrentUsers").load("/handlers/fsobjects.ashx?q=currentusers&selectedObjectType=" + $selectedFSOtype + 
                            "&relpath=" + $selectedFSOpath, {u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' });
                    });

                    // clear the email address text box
                    $('#modalShareNewShareUser').val('')
                }
                
                return false;
            });

            // clear the results if another user is about to be added
            $('#modalShareNewShareUser').on('focus', function() { $("#modalShareAddUserResults").text(''); });

            //////////////////////////////////////////////////////////////////////////////////////
            // this stuff clears out the values of inputs.  probably an easier way to do this.
            $('#modalShare').on('hide', function () {                  
                //$('input,textarea,select').val('');
                $('#modalShareFolderName').text('');
                $('#modalShareCurrentUsers').text('');
                $('#modalShareNewShareUser').val('');
                $('#modalShareAddUserResults').text('');
                $('#optionsAccessLevel').val("R");
            });

            $('#modalNewFolder').on('hide', function () {                 
                $('#modalNewFolderName').val('');
                $('#modalNewFolderResults').text('');
            });

            $('#modalDelete').on('hide', function () {                 
                $('#modalDeleteObjName').text('');
                $('#modalDeleteResults').text('');
            });

            $('#modalRename').on('hide', function () {                 
                $('#modalRenameCurrentName').text('');
                $('#modalRenameNewName').val('');
                $('#modalRenameResults').text('');
            });
            
            $('#modalPublicLink').on('hide', function () {                 
                $('#modalPublicLinkNewLink').text('');
            });

            $('#modalFileUp').on('hide', function () {
                //$(".statusbar").children().remove();
                //$(".statusbar").hide();
                $(".statusbar").remove();
            });

            $('#modalFeedback').on('hide', function () {                 
                $('#feedback').val('');
                $('#feedback').removeAttr('disabled');
                $('#submitFeedback').show();
                $('#modalFeedbackResults').text('');                
            });

            $('#modalNewFile').on('hide', function () {
                $('#newFileName').val('');
            });
            
        });     
    </script>

    <script type="text/javascript">
        function sendFileToServer(formData, status) {
            var uploadURL = '/handlers/fsobjects.ashx?q=upload&relpath=<%=Server.UrlEncode(_relpath)%>'; //Upload URL
            var extraData = { u: '<%=Session["sesUsername"].ToString() %>', sesID: '<%=Session["sesID"].ToString() %>' }; //Extra Data.
            var jqXHR = $.ajax({
                xhr: function () {
                    var xhrobj = $.ajaxSettings.xhr();
                    if (xhrobj.upload) {
                        xhrobj.upload.addEventListener('progress', function (event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }
                            //Set progress
                            status.setProgress(percent);
                        }, false);
                    }
                    return xhrobj;
                },
                url: uploadURL,
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: formData,
                success: function (data) {
                    status.setProgress(100);
                    //$("#status1").append("File upload Done<br>");
                }
            });

        status.setAbort(jqXHR);
    }

    var rowCount = 0;

    function createStatusbar(obj) {
        rowCount++;
        var row = "odd";
        if (rowCount % 2 == 0) row = "even";
        this.statusbar = $("<div class='statusbar " + row + "'></div>");
        this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
        this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
        this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
        this.abort = $("<div class='abort'>Abort</div>").appendTo(this.statusbar);
        obj.after(this.statusbar);

        this.setFileNameSize = function (name, size) {
            var sizeStr = "";
            var sizeKB = size / 1024;
            if (parseInt(sizeKB) > 1024) {
                var sizeMB = sizeKB / 1024;
                sizeStr = sizeMB.toFixed(2) + " MB";
            }
            else {
                sizeStr = sizeKB.toFixed(2) + " KB";
            }

            this.filename.html(name);
            this.size.html(sizeStr);
        }
        this.setProgress = function (progress) {
            var progressBarWidth = progress * this.progressBar.width() / 100;
            this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
            if (parseInt(progress) >= 100) {
                this.abort.hide();
            }
        }
        this.setAbort = function (jqxhr) {
            var sb = this.statusbar;
            this.abort.click(function () {
                jqxhr.abort();
                sb.hide();
            });
        }
    }

    function handleFileUpload(files, obj) {
        for (var i = 0; i < files.length; i++) {
            var fd = new FormData();
            fd.append('file', files[i]);

            var status = new createStatusbar(obj); //Using this we can set progress.
            status.setFileNameSize(files[i].name, files[i].size);
            sendFileToServer(fd, status);

        }
    }

    $(document).ready(function () {
        var obj = $("#dragandrophandler");

        obj.on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).css('border', '2px solid #aaa');
        });

        obj.on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

        obj.on('drop', function (e) {

            $(this).css('border', '2px dashed #aaa');
            e.preventDefault();
            var files = e.originalEvent.dataTransfer.files;

            //We need to send dropped files to Server
            handleFileUpload(files, obj);
        });

        $(document).on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

        $(document).on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            obj.css('border', '2px dashed #aaa');
        });

        $(document).on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

    });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentMain" Runat="Server">
  <div class="container-fluid">  
     <div class="row-fluid">
       <div class="span2">

        <br />
        <div class="btn-group-vertical">
            <a class="btn listbox-CSU" href="home" style="text-align: left; margin-bottom: 8px;">
                <span class="icon-home-3" style="padding-right: 5px;"></span>Home
            </a>
            <a class="btn listbox-CSU" href="home?type=D&relpath=<%=_favoritesPath %>" title="Items tagged as favorites" style="text-align: left; margin-bottom: 8px;">
                <span class="icon-bookmark-3" style="padding-right: 5px;"></span>Favorites
            </a>
            <a class="btn listbox-CSU" href="home?type=D&relpath=<%=_publicPath %>" title="Items viewable by the public" style="text-align: left; margin-bottom: 8px;">
                <span class="icon-globe-2" style="padding-right: 5px;"></span>Public
            </a>
            <a class="btn listbox-CSU" href="home?type=D&relpath=<%=_sharedwithmePath %>" title="Items shared with me" style="text-align: left; margin-bottom: 8px;">
                <span class="icon-users-2" style="padding-right: 5px;"></span>Shared With Me
            </a>
            <a class="btn listbox-CSU" href="home?type=D&relpath=<%=_pixGallery %>" title="Your personal photo gallery" style="text-align: left; margin-bottom: 8px;">
                <span class="icon-pictures" style="padding-right: 5px;"></span>Gallery
            </a>
            <a class="btn listbox-CSU" href="#" id="btnLeaveFeedback" title="Leave feedback" style="text-align: left; margin-bottom: 8px;">
                <span class="icon-comment-3" style="padding-right: 5px;"></span>Leave Feedback
            </a>

            <div class="btn-group listbox-CSU">
                <button class="btn listbox-CSU" style="font-weight: 100;"><span class="icon-video" style="padding-right: 5px;"></span>Tutorials&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                <button class="btn btn-small listbox-CSU dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <li><a href="public/add_to_faves.mp4">Add to favorites</a></li>
                        <li><a href="public/cyberduck_ftps.mp4">Connect with Cyberduck</a></li>
                        <li><a href="public/create_folder.mp4">Create a folder</a></li>
                        <li><a href="public/delete_files.mp4">Delete files</a></li>
                        <li><a href="public/get_public_link.mp4">Get public link</a></li>                        
                        <li><a href="public/play_music.mp4">Music Player</a></li>                        
                        <li><a href="public/share_sender.mp4">Sharing - part 1 </a></li>
                        <li><a href="public/share_recipient.mp4">Sharing - part 2</a></li>
                        <li><a href="public/file_upload.mp4">Upload a file</a></li>
                        <li><a href="public/2013-05-30 14h07_04.mp4">Website Creation - part 1</a></li>
                        <li><a href="public/2013-08-20 14h59_14.mp4">Website Creation - part 2</a></li>
                </ul> 
            </div>
        </div>        
       </div>

       <div class="span10" style="padding-left: 10px;">
        <ul class="breadcrumb" style="">
            <li><a href="home">Home</a></li>
            <asp:Localize ID="locBreadcrumbs" runat="server" />
            <span id="clipboardbox" class="pull-right" style="font-size: 8pt; display: none;"><b>Clipboard: </b>
                <span id="clipboardcontents"></span>&nbsp;&nbsp;[
                <a href="#" id="moveBtn" title="Move item here">move</a> |
                <a href="#" id="copyBtn" title="Copy item here">copy</a> |
                <a href="#" id="clearBtn" title="Clear clipboard contents">clear</a>&nbsp;&nbsp;]
            </span>
        </ul>

        <div id="fsobjecttable" style="margin-top: -25px; margin-bottom: 75px;"></div>
        
       </div>
     </div>
   </div>


</asp:Content>

