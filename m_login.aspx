﻿    <%@ Page Language="C#" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.DirectoryServices" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web.SessionState" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Text" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    //Start Login Form Code - Assuming your are using the standard ASP.NET login control with ID="Login" and OnAuthenticate="OnAuthenticate"
    [System.Runtime.InteropServices.DllImport("advapi32.dll", SetLastError = true)]
    private static extern bool LogonUser(
        string lpszUsername,
        string lpszDomain,
        string lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        out IntPtr phToken
    );

    protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
    {
        IntPtr loginToken;
        e.Authenticated = LogonUser(this.Login1.UserName, "CCSU", this.Login1.Password, 8, 0, out loginToken);
        if (e.Authenticated)
        {
            LochBoxHelpers lochy = new LochBoxHelpers();

            this.Session["LoginToken"] = loginToken;
            Session["sesUsername"] = Login1.UserName.ToLower();
            Session["sesPasswdKey"] = Login1.Password.Crypt();

            string strRoles = "NONE";

            // connect to directory and get some user info...
            DirectoryEntry deMain = new DirectoryEntry("<LDAP_PATH>");
            deMain.Username = WebConfigurationManager.AppSettings["dirreadUsername"];
            deMain.Password = WebConfigurationManager.AppSettings["dirreadPassword"];

            DirectorySearcher dsMain = new DirectorySearcher(deMain);
            dsMain.Filter = "(& (sAMAccountName=" + Session["sesUsername"].ToString() + ") (objectClass=user))";
            SearchResult srUser = dsMain.FindOne();
            DirectoryEntry deUser = srUser.GetDirectoryEntry();

            // now populate session variables with user info...eliminates tons of lookups        
            Session["sesEmail"] = deUser.Properties["mail"].Value.ToString();
            Session["sesFullname"] = deUser.Properties["displayName"].Value.ToString();

            // create a session id for this session
            Session["sesID"] = HttpContext.Current.Session.SessionID;

            //update the session keeper with the session id
            updateSessionKeeper(Session["sesID"].ToString(), Session["sesUsername"].ToString());

            // build the group role arraylist    
            ArrayList alGroups = new ArrayList();

            foreach (object objGroup in deUser.Properties["memberOf"])
            {
                // add each group as to the arraylist                
                string group = objGroup.ToString().ToLower().Replace("cn=", @"ccsu\");

                alGroups.Add(group.Substring(0, group.IndexOf(',')));
            }

            Session["sesRoles"] = alGroups;

            // set the path for either employees or students
            if (deUser.Properties["extensionAttribute10"].Value.ToString() == "Employee" || deUser.Properties["extensionAttribute10"].Value.ToString() == "Student")
                Session["sesUserDesc"] = deUser.Properties["extensionAttribute10"].Value.ToString().ToLower();
            else
                Response.Redirect("/logout", true);

            deUser.Close();
            deMain.Close();

            // make sure _SharedWithMe special folder exists
            if (!Directory.Exists(Application["basePath"].ToString() + Session["sesUsername"].ToString() + "\\_SharedWithMe"))
                Directory.CreateDirectory(Application["basePath"].ToString() + Session["sesUsername"].ToString() + "\\_SharedWithMe");

            // make sure _Favorites special folder exists
            if (!Directory.Exists(Application["basePath"].ToString() + Session["sesUsername"].ToString() + "\\_Favorites"))
                Directory.CreateDirectory(Application["basePath"].ToString() + Session["sesUsername"].ToString() + "\\_Favorites");

            // log this login
            logIt("Login", "", "SUCCESS", this.Login1.UserName, Request.ServerVariables["Remote_Host"].ToString());

            Login1.DestinationPageUrl = "/m";
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        //Response.Write(Request.ServerVariables["HTTP_USER_AGENT"].ToString());
        
        // if the user tried to go inside the site without logging in, remember where they were going
        if (Request.QueryString["returnurl"] != null && Session["sesUsername"] != null)
            Response.Redirect(Request.QueryString["returnurl"], true);
    }

    public void updateSessionKeeper(string strSesID, string strUsername)
    {
        // track this session in a database table
        MySqlConnection con = new MySqlConnection(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);
        string sql = "insert into websessions (Session_ID,username) values ('" + strSesID + "', '" + strUsername + "')";
        con.Open();
        MySqlCommand cmd = new MySqlCommand(sql, con);
        cmd.ExecuteNonQuery();
        con.Close();
        con.Dispose();
    }

    private void logIt(string function, string data, string response, string username, string remotehost)
    {
        // log the transactions
        MySqlConnection con = new MySqlConnection(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);

        string sql = "insert into accesslog (function,data,response,username,remote_host) " +
            "values ('" + function + "','" + data + "', '" + response + "', '" + username + "','" + remotehost + "')";

        MySqlCommand cmd = new MySqlCommand(sql, con);
        con.Open();
        cmd.ExecuteNonQuery();

        con.Close();
    } 
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <title>CSU Lochbox Mobile</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
   <link rel="shortcut icon" href="resources/ico/favicon.ico" />
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="resources/img/folder-lock-icon.png" />
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/img/folder-lock-icon.png" />
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="resources/img/folder-lock-icon.png" />
   <link rel="apple-touch-icon-precomposed" href="resources/ico/favicon.ico" />

   <!-- style -->
   <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css" />
   <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-responsive.css" />
   <link rel="stylesheet" type="text/css" href="resources/css/bootmetro.css" />
   <link rel="stylesheet" type="text/css" href="resources/css/metro-ui-light.css" />
   <link rel="stylesheet" type="text/css" href="resources/css/icomoon.css" />

    <script type="text/javascript">
        $(document).ready(function () {
            // off be with you address bar..
            //$('body').scrollTop(1);  

            //$("#Login1_UserName").attr("placeholder", "Enter username...");
        });

    </script>

</head>

<body data-accent="blue">
    <form id="form1" runat="server">
    <div style="width: 100%;">
        <div style="width: 80%; margin: 0px auto;">
            <h3>CSU Lochbox Mobile</h3>            
            <div class="well-small" style="background-image:url('resources/img/lochbox-8.jpg'); background-repeat:no-repeat; background-position: -15px 0px">
                    <asp:login id="Login1" runat="server" TitleText="" DisplayRememberMe="false" FailureText="Incorrect username or password!" 
                        OnAuthenticate="OnAuthenticate" Orientation="Vertical" TextLayout="TextOnTop" UserNameLabelText="Username:&nbsp;&nbsp;" 
                        PasswordLabelText="Password:&nbsp;&nbsp;" UserNameRequiredErrorMessage="User name is required.">                    
                        <LoginButtonStyle CssClass="btn btn-primary" />
                        <FailureTextStyle  />
                    </asp:login>    
            </div>
        </div>
    </div>
    </form>
</body>
</html>
