$(function () {

    var dropbox = $('#dropbox'),
		message = $('.message', dropbox);

    dropbox.filedrop({
        maxfiles: 5,
        maxfilesize: 25,
        url: '/handlers/fileup.ashx',

        uploadFinished: function (i, file, response) {
            $.data(file).addClass('done');
        },

        error: function (err, file) {
            switch (err) {
                case 'BrowserNotSupported':
                    showMessage('Your browser does not support HTML5 file uploads!');
                    break;
                case 'TooManyFiles':
                    alert('Too many files! Please select 5 at most! (configurable)');
                    break;
                case 'FileTooLarge':
                    alert(file.name + ' is too large! Maximum file size is 25MB.');
                    break;
                default:
                    break;
            }
        },

        uploadStarted: function (i, file, len) {
            createImage(file);
        },

        progressUpdated: function (i, file, progress) {
            $.data(file).find('.progress').width(progress);
        }

    });

    var template = '<div class="preview">' +
                        '<span class="thefilename"></span><br />' +
						'<div class="progressHolder">' +
							'<div class="progress"></div>' +
						'</div>' +
						'<span class="imageHolder">' +
							'<span class="uploaded"><b>Done</b></span>' +
						'</span>' +
					'</div>';


    function createImage(file) {

        var preview = $(template),
			image = $('img', preview);

        var reader = new FileReader();

        reader.onload = function (e) {
            $('.thefilename').text(file.name);
        };

        reader.readAsDataURL(file);

        message.hide();
        preview.appendTo(dropbox);

        $.data(file, preview);
    }

    function showMessage(msg) {
        message.html(msg);
    }

});