﻿<%@ Master Language="C#" %>
<%@ Import Namespace="System.DirectoryServices" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    public void Page_Load(Object source, EventArgs e)
    {
        // hide the menu links if user not logged in...
        if (Session["sesUsername"] != null)
        {
            pnlMenuLinks.Visible = true;
        }
        else
        {
            pnlMenuLinks.Visible = false;
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Wemedy</title>

    <link href="/wemedy.css" rel="stylesheet" type="text/css" />
    <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="vallenato/vallenato.css" type="text/css" media="screen" />

    <script type="text/javascript">
        //var x = -40; //Starting Location - left
        //var y = 200; //Starting Location - top
        var dest_x = 1000;  //Ending Location - left
        var dest_y = 200;  //Ending Location - top
        var interval = 1; //Move 10px every initialization

        function moveZombie() {
            //Keep on moving the image till the target is achieved
            if (x < dest_x) x = x + interval;
            //if (y < dest_y) y = y + interval;

            //Move the image to the new location
            //document.getElementById("mummy").style.top = '17px';
            document.getElementById("mummy").style.left = x + 'px';

            //if ((x + interval < dest_x) && (y + interval < dest_y)) {
            if (x + interval < dest_x) {
                //Keep on calling this function every 100 microsecond 
                //    till the target location is reached
                window.setTimeout('moveImage()', 50);
            }
        }
    </script>

    <asp:ContentPlaceHolder id="head" runat="server" />
    
</head>

<body onload="moveZombie()">
    <form id="form1" runat="server">
   
        <div id="mummy" style="position: absolute;">
            <img src="/images/halloween/z2.gif" width="64px" height="84px" alt="Z2" title="Brains!!!!!!" style="left: -30px; top: 17px;"  />
        </div>

        <div class="topdiv">
            <br />
            <h1 style="position: absolute; top: 5px; left: 60px;">::&nbsp;&nbsp;Wemedy&nbsp;&nbsp;::</h1>
        </div>

        <!-- MENU BAR -->
        <div id="mb" style="background-color: RGB(92,105,135); width: 100%; height: 40px; color: White; font-family: sans-serif; font-size: 10pt;">
            <div style="width: 80%; margin-left: 100px; padding-top: 20px;">
                <asp:Panel ID="pnlMenuLinks" runat="server">
                    <span style="padding-right: 50px;"><a href="#" title="Agent Presence" class="a-menu">Agent Presence</a></span>   
                    <span style="padding-right: 50px;"><a href="exch.aspx" title="Exchange Status" class="a-menu" id="menuExchange">Exchange Status</a></span> 
                    <span style="padding-right: 50px;"><a href="hr.aspx" title="HR" class="a-menu" id="menuHR">HR</a></span>      
                    <span style="padding-right: 50px;"><a href="ipman.aspx" title="IP Address Manager"class="a-menu" id="menuIPMan">IP Address Manager</a></span>
                    <span style="padding-right: 50px;"><a href="events.aspx" title="Network Events" class="a-menu" id="menuEvents">Network Events</a></span> 
                    <span style="padding-right: 50px;"><a href="wa.aspx" title="Workstation Auditor" class="a-menu" id="menuWA">Workstation Auditor</a></span>                     
                </asp:Panel>               
            </div>            
        </div> 


        <div class="maincontainer">        
            <br />
            <div id="loading" class="loading-invisible">
                <p><span id="loadingMessage"></span>.&nbsp;<img src="images/l1.gif" width="16px" /></p>
            </div>
            <asp:ContentPlaceHolder id="ContentPlaceHolder1" runat="server" />        
        </div>

    </form>
</body>
</html>
