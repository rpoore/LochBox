﻿<%@ Page Language="C#" ValidateRequest="false" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    
    private void Page_Load(object sender, System.EventArgs e)
    {

    }
    
    private string GetShortenedURL(string inURL)
    {
        // returns the homemade url shortener thingy ma bob
        string shortURL = inURL;
        inURL = shortURL;

        string queryURL = "https://lochbox.clayton.edu/publicwebservices/url.asmx/getShortURL?strOriginalURL=" + inURL.Replace("https://lochbox.clayton.edu/", "");

        WebClient wc = new WebClient();
        Stream data = wc.OpenRead(queryURL);
        StreamReader reader = new StreamReader(data);
        shortURL = reader.ReadToEnd();
        data.Close();
        reader.Close();

        return shortURL;
    }

    protected void btnGetURL_Click(object sender, EventArgs e)
    {
        lblResults.Text = GetShortenedURL(txtLongURL.Text);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtLongURL" runat="server" />
        <asp:Button ID="btnGetURL" runat="server" Text="Get Short URL" 
            onclick="btnGetURL_Click" />
        <br />
        <asp:Label ID="lblResults" runat="server" />
    </div>
    </form>
</body>
</html>
