﻿<%@ WebHandler Language="C#" Class="gallery" %>

using System;
using System.IO;
using System.Web;

public class gallery : IHttpHandler 
{    
    public void ProcessRequest (HttpContext context) 
    {
        string response = null;
        string url = context.Request.Form["url"];                   
        string username = url.Replace("https://lochbox.clayton.edu/users/","").Replace("/gallery/index.html","");
        string basePath = @"D:\Websites\lochbox.clayton.edu\users\public\" + username + @"\Gallery\albums";
        
        if (context.Request.Form["q"].ToString() == "listalbums")
            response = ListAlbums(basePath);
        else if (context.Request.Form["q"].ToString() == "showalbumpics")
            response = showAlbumPics(basePath, context.Request.Form["albumname"].ToString());
        
        //else
        //    response = "fred the cat";
        //string response = "fred the cat";
        
        context.Response.ContentType = "text/plain";
        context.Response.Write(response);
    }

    private string ListAlbums(string path)
    {
        string returnValue = null;
        
        DirectoryInfo dirInfo = new DirectoryInfo(path);
        DirectoryInfo[] rgDirs = dirInfo.GetDirectories("*.*");

        foreach (DirectoryInfo di in rgDirs)
        {
            returnValue += "<li><span class='albumname' data-album='" + di.Name + "'>" + di.Name + "</span></li>";
        }

        return returnValue;
    }    

    private string showAlbumPics(string path, string albumName)
    {
        string returnValue = null;
        
        DirectoryInfo dirInfo = new DirectoryInfo(path + "\\" + albumName);
        FileInfo[] rgFiles = dirInfo.GetFiles("*.*");

        foreach (FileInfo fi in rgFiles)
        {
            if (fi.Name.ToLower().EndsWith(".jpg") || fi.Name.ToLower().EndsWith(".bmp") || fi.Name.ToLower().EndsWith(".png"))
            {
                returnValue += "<a href=\"albums/" + albumName + "/" + fi.Name + "\" data-lightbox=\"image-1\" title=\"" + fi.Name + "\">";
                returnValue += "<img src=\"albums/" + albumName + "/" + fi.Name + "\" width=\"120px\" /></a>";
            }
        }
  
        return returnValue;
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}