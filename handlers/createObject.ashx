﻿<%@ WebHandler Language="C#" Class="createObject" %>

using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.SessionState;

public class createObject : IHttpHandler 
{    
    public void ProcessRequest (HttpContext context) 
    {
        string returnValue = null;
        context.Response.ContentType = "text/plain";
        
        // end this if it's not a proper call
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("INVALID_REQUEST");
            context.Response.End();
        }

        ArrayList alGroups = (ArrayList)context.Session["sesRoles"];
        LochBoxHelpers lochy = new LochBoxHelpers();

        if (lochy.verifySession(context.Request.Form["sesID"]))
        {
            string type = context.Request.Form["selectedObjectType"];
            string relpath = context.Server.UrlDecode(context.Request.Form["relpath"]);
            string path = lochy.BaseHomeFolderPath + relpath;
            string nfname = context.Server.UrlDecode(context.Request.Form["nfname"]);

            // get read/write access
            lochy.getReadWriteAccess(path, context.Request.Form["u"], alGroups);            

            if (type == "D")
            {
                // type is directory
                if (Directory.Exists(path + "\\" + nfname))
                {
                    returnValue = "Directory already exists.";
                }
                else
                {
                    if (lochy.HasWriteAccess || relpath.ToLower() == context.Request.Form["u"].ToLower())                     
                    {
                        Directory.CreateDirectory(path + "\\" + nfname);
                        returnValue = "Directory created!";
                        lochy.logIt("createFolder", context.Request.Form["u"], nfname, "OK", context.Request.Form["remotehost"]);
                    }
                    else
                    {
                        returnValue = "Access denied.";
                    }
                }                
            }
            else
            {
                // type must be file
                if (File.Exists(path + "\\" + nfname))
                {
                    returnValue = "File already exists.";
                }
                else
                {
                    if (lochy.HasWriteAccess || relpath.ToLower() == context.Request.Form["u"].ToLower())
                    {
                        StreamWriter sw = new StreamWriter(path + "\\" + nfname, false);
                        sw.Close();
                        returnValue = "File created.";
                        lochy.logIt("createFile", context.Request.Form["u"], nfname, "OK", context.Request.Form["remotehost"]);                        
                    }
                    else
                    {
                        returnValue = "Access denied.";
                    }
                }              
            }
        }
        else
        {
            returnValue = "INVALID_SESSION";
        }
        
        context.Response.Write(returnValue);
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}