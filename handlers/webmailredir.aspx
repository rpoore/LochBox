﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Web.Configuration" %>

<!DOCTYPE html>

<script runat="server">
    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Session["sesUsername"] != null && Session["sesPasswdKey"] != null)
        {   
            username.Text = Session["sesUsername"].ToString();
            password.Text = Session["sesPasswdKey"].ToString().Decrypt();           
        }  
        else
            Response.Redirect("/login?returnurl=" + Server.UrlEncode(Request.Url.ToString()), true);      
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
           $("[id*='logonForm']").hide();
           $("[id*='submit']").click();
        });
    </script>

</head>
<body>
    <form id="logonForm" name="logonForm" runat="server" action="https://lochmail.clayton.edu/owa/auth.owa" method="POST" enctype="application/x-www-form-urlencoded">
    <div>
        Redirecting.....
        <div>
            <asp:TextBox ID="destination" runat="server">https://lochmail.clayton.edu/owa/?authRedirect=true</asp:TextBox>
            <asp:TextBox ID="flags" runat="server">4</asp:TextBox>
            <asp:TextBox ID="forcedownlevel" runat="server">0</asp:TextBox>
            <asp:TextBox ID="trusted" runat="server">4</asp:TextBox>
            <asp:TextBox ID="isUtf8" runat="server">1</asp:TextBox>

            <asp:TextBox ID="username" runat="server"></asp:TextBox>
            <asp:TextBox ID="password" runat="server"></asp:TextBox>

            <asp:Button ID="submit" runat="server"/> 
        </div>  
    </div>
    </form>
</body>
</html>
