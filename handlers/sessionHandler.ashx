﻿<%@ WebHandler Language="C#" Class="sessionHandler" %>

using System;
using System.Web;
using System.Web.SessionState;

public class sessionHandler : IHttpHandler 
{    
    public void ProcessRequest (HttpContext context) 
    {
        // only accept valid requests
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("Invalid Request");
            context.Response.End();
        }
        //////////////////////////////////////////////////////////////////

        string returnValue = null;
        
        // check for session timeout
        if (String.IsNullOrWhiteSpace(context.Session["sesUsername"].ToString()))
            returnValue = "SESSION_INVALID";
        else
            returnValue = "SESSION_VALID";
 
        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}