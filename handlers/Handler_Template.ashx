﻿<%@ WebHandler Language="C#" Class="Handler_Template" %>

using System;
using System.Web;
using System.Web.SessionState;

public class Handler_Template : IHttpHandler 
{   
    public void ProcessRequest (HttpContext context) 
    {
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("Invalid Request");
            context.Response.End();
        }
        else
        {
            string returnValue = "";

            try
            {

                returnValue = "";
            }
            catch (Exception x)
            {
                returnValue += x.Message;
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(returnValue);
        }
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}