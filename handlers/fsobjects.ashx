﻿<%@ WebHandler Language="C#" Class="fsobjects" %>

using System;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.DirectoryServices;
using System.Web.SessionState;
using System.Security.AccessControl;
using System.Web.Configuration;
using LochBox;

public class fsobject
{
    public string NAME { get; set; }
    public string PATH { get; set; }
    public string SIZE { get; set; }
    public string LASTMODIFIED { get; set; }
    public char TYPE { get; set; }
    public string ICON { get; set; }
    public string MUSICPATH { get; set; }
}

public class ViewDataUploadFilesResult
{
    public string Thumbnail_url { get; set; }
    public string Name { get; set; }
    public int Length { get; set; }
    public string Type { get; set; }
}

public class fsobjects : IHttpHandler, IRequiresSessionState
{
    string _path = null;
    string _username = null;
    string _sesID = null;
    string _groups = null;
    string _basePath = null;
    string _baseEmployeePath = null;
    string _baseStudentPath = null;
    string _remotehost = null;
    string _dbconnstring = null;  
    string serJSON = null;
    JavaScriptSerializer _javaScriptSerializer = null;
    DirectoryEntry deMain = null;
           
    public void ProcessRequest (HttpContext context) 
    {
        // only accept valid requests
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("Invalid Request");
            context.Response.End();
        }
        //////////////////////////////////////////////////////////////////
           
        // make sure the request has the barest of essentials...
        // ------------------------------------------------------------------------------ //
        if (String.IsNullOrWhiteSpace(context.Session["sesUsername"].ToString()))
            context.Response.End();
        
        // othewerise, we continue...        
        // set the main vars
		LochBoxHelpers lochy = new LochBoxHelpers();
		
        _username = context.Session["sesUsername"].ToString();
        _sesID = context.Request.Form["sesID"];
        //_groups = context.Session["sesRoles"].ToString();  
              
		_basePath = lochy.BaseHomeFolderPath;   
        _dbconnstring = WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;        
        _javaScriptSerializer = new JavaScriptSerializer();
        _remotehost = context.Request.ServerVariables["REMOTE_HOST"].ToString();
        
        deMain = new DirectoryEntry();
        deMain.Path = "LDAP://ccsunet.clayton.edu/dc=ccsunet,dc=clayton,dc=edu";
        deMain.Username = WebConfigurationManager.AppSettings["dirreadUsername"];
        deMain.Password = WebConfigurationManager.AppSettings["dirreadPassword"];                           
                            
        // set the full path 
        // ------------------------------------------------------------------------------ // 
        if (context.Request.QueryString["relpath"] != null)
        {
            // the relpath should always be urlencoded on the querystring
            _path = _basePath + context.Server.UrlDecode(context.Request.QueryString["relpath"]);
        }
        else
            _path = _basePath + _username;
        // ------------------------------------------------------------------------------ //         
        
        // now see what needs to be done 
        // ------------------------------------------------------------------------------ // 
        if (context.Request.QueryString["q"].ToString() != null)
        {
            string query = context.Request.QueryString["q"].ToString();
            
            // get the list of current users
            if (query == "currentusers")
                getCurrentUsers(context);  
            
            // add user to folder
            if (query == "adduser")
                addUser(context);

            // remove user from folder
            if (query == "removeuser")
                removeUser(context);              
            
            // get public link
            if (query == "getpubliclink")
                getPublicLink(context);
            
            // email public link
            if (query == "sendpublink")
                sendPublicLink(context);
            
            // stuff that depends on the path...make sure logged in user is accessing right path
            if (_path.StartsWith(_basePath + context.Session["sesUsername"].ToString()))
            {
                switch (query)
                {
                    case "createobject":
                        createObject(context);
                        break;
                    case "deleteobject":
                        deleteObject(context);
                        break;
                    case "list":
                        listFSObjects(context);
                        break;                        
                    case "openfile":
                        openTextFile(context);
                        break;
                    case "renobject":
                        renameObject(context);
                        break;
                    case "savefile":
                        saveTextFile(context);
                        break;
                    case "upload":
                        uploadFile(context);
                        break;
                    default:
                        break;
                }
            }            
        }
    }
    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//
    
    
    //----------------------------------------------------------------------------------//
    private void addUser(HttpContext context)
    {
        string returnValue = null;
        string userToAdd = context.Request.QueryString["usertoadd"].ToString();
        string accesslevel = context.Request.QueryString["accesslevel"].ToString();        
        
        // get the username from the passed user and make sure it's valid
        //     can be either e-mail address or username
        string recipientaccount = validateRecipientAccount(userToAdd);

        if (recipientaccount != "ACCOUNT_NOT_VALID")
        {
            if (applyPermissions(_path, recipientaccount, accesslevel))
            {
                returnValue = "<div class='alert alert-success'>Permission granted for " + recipientaccount + "</div><br />";

                // get the shared folder name
                string foldername = _path.Substring(_path.LastIndexOf(@"\") + 1);

                // make the _SharedWithMe link for the recipient
                //string recipientSharedWithMePath = @"D:\Websites\lochbox.clayton.edu\users\home\" + recipientaccount + @"\_SharedWithMe";
                string recipientSharedWithMePath = _basePath + recipientaccount + @"\_SharedWithMe";

                // create the _SharedWithMe folder for the recipient if not there already
                if (!Directory.Exists(recipientSharedWithMePath))
                    Directory.CreateDirectory(recipientSharedWithMePath);

                // make the sym link 
                string symlinkname = recipientSharedWithMePath + @"\" + _username + "_" + foldername.Replace(" ", "_");

                string cmd = "/c mklink /D \"" + symlinkname + "\" \"" + _path + "\"";

                if (runCommand(cmd))
                {
                    // process did run
                    //returnValue += "<div class='alert alert-success' style='top: -15px;'>Symbolic link created for " + recipientaccount + "</div>";
                    logIt("makeSymLink", cmd, "SUCCESS");
                }
                else
                {
                    // did not make the symlink
                    //returnValue += "<div class='alert alert-error' style='top: -15px;'>Failed creating symbolic link for " + recipientaccount + "</div>";
                    logIt("makeSymLink", cmd, "FAILED");
                }
                
                // finally, e-mail the user
                //string sharedfolderurl = "https://lochbox.clayton.edu/home?type=D&relpath=" + recipientaccount + "%5c_SharedWithMe%5c" + _username + "_" + foldername.Replace(" ", "_");
                            
                string subject = "New Lochbox share...";
                string recipientEmail = getUserInfo("getEmailFromUsername", recipientaccount);
                string body = _username + " has shared \"" + foldername + "\" with you. " +
                    "You should see this folder under your \"Shared With Me\" folder after logging into " +
                    "https://lochbox.clayton.edu";

                if (sendEmail(recipientEmail, context.Session["sesEmail"].ToString(), subject, body))
                {
                    returnValue += "<div class='alert alert-success' style='top: -30px;'>E-mail sent to " + recipientaccount + "</div>";
                    logIt("emailRecipient", recipientEmail, "SUCCESS");
                }
                else
                {
                    returnValue += "<div class='alert alert-error' style='top: -30px;'>Failed e-mailing " + recipientaccount + "</div>";
                    logIt("emailRecipient", recipientEmail, "FAILED");
                }
            }
            else
            {
                // failed adding permission!
                returnValue = "<div class='alert alert-error'>Failed adding permission for " + recipientaccount + "</div>";
            }
        }
        else
        {
            // account not found!
            returnValue = "<div class='alert alert-error'>That's not a valid CSU account!</div>";
        }

        context.Response.ContentType = "text/html";
        context.Response.Write(returnValue);
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void renameObject(HttpContext context)
    {
        string returnValue = null;
        string selectedObjectType = context.Request.QueryString["selectedObjectType"].ToString();
        string newname = context.Request.QueryString["nfname"].ToString();

        // todo:  need to strip the folder or file name from the path
        string strippedPath = _path.Substring(0,_path.LastIndexOf(@"\"));

        if (hasWriteAccess(_username, _path))
        {
            // if it's a folder delete it
            if (selectedObjectType == "D")
            {
                Directory.Move(_path, strippedPath + @"\" + newname);
            }
            else
            {
                // must be a file
                File.Move(_path, strippedPath + @"\" + newname);
            }

            returnValue = "Object renamed.";
            //returnValue = strippedPath;

            // log this
            logIt("renameObject", _path.Replace(@"\", "/"), "OK");
        }
        else
        {
            returnValue = "Access Denied!";
            logIt("renameObject", _path.Replace(@"\", "/"), "ACCESS_DENIED");
        }  
        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    } 
    
    private void getCurrentUsers(HttpContext context)
    {
        string returnValue = null;

        // read the acl's and see if the user has access
        DirectoryInfo dInfo = new DirectoryInfo(_path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower().Replace("ccsu\\","");
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            if (!identity.Contains("webfilemanager") && !identity.Contains("csubackupaccount") && !identity.Contains("iusr") && !identity.Contains("domain admins") && !identity.Contains(_username))
            {
                returnValue += identity + ": " + accesslevel.Replace("modify, synchronize", "Read and Write").Replace("read, synchronize","Read").Replace("fullcontrol", "Read and Write");
                returnValue += " [<a href='#' class='removeuser' data-user='" + identity + "' title='Remove this user' style='color: #ff4444;'>x</a>]<br />";
            }
        }

        context.Response.ContentType = "text/html";
        context.Response.Write(returnValue); 
    }

    private void removeUser(HttpContext context)
    {
        string returnValue = null;       
        string otheruser = context.Request.QueryString["otheruser"];
        string remotehost = context.Request.Form["remotehost"];

        fsomanager fsoman = new fsomanager();
        returnValue = fsoman.removeUser(_path, _username, _sesID, otheruser, remotehost);        

        context.Response.ContentType = "text/html";
        context.Response.Write(returnValue);
    }    

    private void listFSObjects (HttpContext context)
    {    
        DirectoryInfo dirInfo = new DirectoryInfo(_path);
        DirectoryInfo[] rgDirs = dirInfo.GetDirectories("*.*");
        FileInfo[] rgFiles = dirInfo.GetFiles("*.*");

        fsobject _fsobject = null;
        List<fsobject> _fsobjectlist = new List<fsobject>();
        ArrayList alGroups = (ArrayList)context.Session["sesRoles"];
        LochBoxHelpers lochy = new LochBoxHelpers();

        //// add the "up a directory" link
        //_fsobject = new fsobject();
        //_fsobject.NAME = "[ back ]";
        //_fsobject.TYPE = 'D';
        //_fsobject.PATH = null;
        //_fsobject.LASTMODIFIED = null;
        //_fsobject.SIZE = "";
        //_fsobject.ICON = "icon-back-2";
        //_fsobjectlist.Add(_fsobject);
        
        // list directories first
        foreach (DirectoryInfo di in rgDirs)
        {
            // only show if the user has access
            //if (hasListAccess(_username, di.FullName))
            //if (di.FullName.ToLower().StartsWith(@"\\lochbox.clayton.edu\home\" + _username + @"\"))  
            //if (lochy.getReadWriteAccess(di.FullName, _username, alGroups).StartsWith("r"))
            
            // get read/write access
            //lochy.getReadWriteAccess(di.FullName, _username, alGroups);
            //if (lochy.HasReadAccess)
            
            if (getReadWriteAccess(di.FullName, _username, alGroups).StartsWith("r"))
            {
                _fsobject = new fsobject();
                _fsobject.NAME = di.Name;
                _fsobject.TYPE = 'D';
                _fsobject.PATH = context.Server.UrlEncode(di.FullName.Replace(_basePath, "")); // strip the base path
                _fsobject.LASTMODIFIED = di.LastWriteTime.ToShortDateString();
                _fsobject.SIZE = "";                
                _fsobject.ICON = "icon-folder-3";

                _fsobjectlist.Add(_fsobject);
            }           
        }
            
        //list files second
        foreach (FileInfo fi in rgFiles)
        {
            // only show if the user has access
            //if (hasListAccess(_username, fi.FullName))
            //if (fi.FullName.ToLower().StartsWith(@"\\lochbox.clayton.edu\home\" + _username + @"\")) 
            //if (lochy.getReadWriteAccess(fi.FullName, _username, alGroups).StartsWith("r"))

            // get read/write access
            //lochy.getReadWriteAccess(fi.FullName, _username, alGroups);
            //if (lochy.HasReadAccess)            
            if (getReadWriteAccess(fi.FullName, _username, alGroups).StartsWith("r"))
            {
                _fsobject = new fsobject();
                _fsobject.NAME = fi.Name;
                _fsobject.TYPE = 'F';
                _fsobject.PATH = context.Server.UrlEncode(fi.FullName.Replace(_basePath, "")); // strip the base path
                _fsobject.LASTMODIFIED = fi.LastWriteTime.ToShortDateString();
                _fsobject.SIZE = FormatBytes(fi.Length);                
                _fsobject.ICON = getIcon(fi.FullName);
                //_fsobject.ISSHARED = false;

                // more of a pain to try to decode the encoded path with js
                //   so we'll just send the correct path to the client
                if (fi.Name.ToLower().EndsWith(".mp3"))
                    _fsobject.MUSICPATH = fi.FullName.Replace(_basePath, "http://lochbox.clayton.edu/users/").Replace("_Public", "").Replace("_Favorites", "").Replace("\\", "/");

                _fsobjectlist.Add(_fsobject);
            }     
        }

        // log this -- for debugging only
        //logIt("listFSObjects", _path.Replace(@"\","/"), "OK");          

        serJSON = _javaScriptSerializer.Serialize(_fsobjectlist);
        context.Response.ContentType = "applicaton/json";      
        context.Response.Write("{\"Result\":\"OK\", \"Records\": " + serJSON + " }");            
    }

    public void openTextFile(HttpContext context)
    {
        string returnValue = null;

        StreamReader input;

        input = new StreamReader(_path, System.Text.Encoding.ASCII);
        returnValue = input.ReadToEnd();
        input.Close();

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);         
    }       

    
    public void uploadFile(HttpContext context)
    {
        var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();

        JavaScriptSerializer js = new JavaScriptSerializer();

        foreach (string file in context.Request.Files)
        {
            HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
            string FileName = string.Empty;
            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
            {
                string[] files = hpf.FileName.Split(new char[] { '\\' });
                FileName = files[files.Length - 1];
            }
            else
            {
                FileName = hpf.FileName;

                // ios uploads as image.jpg...yeah, this is just a quicky.  will pass iphone user agent eventually
                if (FileName.Equals("image.jpg"))
                {
                    FileName = FileName.Replace("image.jpg", "image_" + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + ".jpg");
                }
            }
            if (hpf.ContentLength == 0)
                continue;

            //string savedFileName = @"D:\Websites\lochbox.clayton.edu\tmpUploads\" + FileName;
            string savedFileName = _path + "\\" + FileName;
            hpf.SaveAs(savedFileName);

            r.Add(new ViewDataUploadFilesResult()
            {
                //Thumbnail_url = savedFileName,
                Name = FileName,
                Length = hpf.ContentLength,
                Type = hpf.ContentType
            });
            var uploadedFiles = new
            {
                files = r.ToArray()
            };
            var jsonObj = js.Serialize(uploadedFiles);

            // now move this file to the right spot
            // if the destination file exists, delete it
            //if (File.Exists(_path + "\\" + FileName))
            //    File.Delete(_path + "\\" + FileName);

            //File.Move(savedFileName, _path + "\\" + FileName);  

            //fsomanager fsoman = new fsomanager();
            //fsoman.moveUploadedFile(savedFileName.Replace("D:\\", @"\\newton.clayton.edu\d$\"), _path + "\\" + FileName);

            context.Response.ContentType = "application/json";
            context.Response.Write(jsonObj.ToString());
        }
    }    

    public void getPublicLink(HttpContext context)
    {
        string fsPath = null;
        string returnValue = null;        

        if (context.Request.QueryString["relpath"] != null)
        {
            fsPath = context.Server.UrlDecode(context.Request.QueryString["relpath"].ToString());
            
            // make sure this is coming from the _Public folder
            if (fsPath.Contains(_username + @"\_Public\"))
            {
                // clean up the path
                // first remove the base server drive path...
                fsPath = fsPath.ToLower().Replace(_basePath, "");

                // slash switch           
                fsPath = fsPath.Replace(@"\", "/");

                // return and remove _public from the link
                returnValue = GetShortenedURL("https://lochbox.clayton.edu/users/" + fsPath.Replace("/_public", ""));
            }
            else
            {
                returnValue = "Public links only work on files under the _Public folder!";
            }
        }
        else
        {
            returnValue = "Missing fso path";
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }

    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//
    // private functions
    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//    
    private bool applyPermissions(string path, string recipientaccount, string permissionlevel)
    {
        bool applyPermissionsSuccess = false;
        string caclsSwitches = null;

        try
        {
            if (permissionlevel != "Z")
                caclsSwitches = @" /grant:r ccsu\" + recipientaccount + ":(OI)(CI)(" + permissionlevel + ") /T";
            else
                caclsSwitches = @" /T /remove:g ccsu\" + recipientaccount;

            string caclsLoc = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "ICACLS.EXE");

            Process p = new Process();

            p.StartInfo.FileName = caclsLoc;
            p.StartInfo.Arguments = "\"" + path + "\"" + caclsSwitches;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            p.StartInfo.CreateNoWindow = true;
            p.Start();

            applyPermissionsSuccess = true;
        }
        catch (Exception x)
        {
            applyPermissionsSuccess = false;
        }

        return applyPermissionsSuccess;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void createObject(HttpContext context)
    {
        string returnValue = null;
        context.Response.ContentType = "text/plain";
        
        ArrayList alGroups = (ArrayList)context.Session["sesRoles"];
        LochBoxHelpers lochy = new LochBoxHelpers();

        if (lochy.verifySession(context.Request.Form["sesID"]))
        {
            string type = context.Request.Form["selectedObjectType"];
            string relpath = context.Server.UrlDecode(context.Request.Form["relpath"]);
            string path = lochy.BaseHomeFolderPath + relpath;
            string nfname = context.Server.UrlDecode(context.Request.Form["nfname"]);

            if (type == "D")
            {
                // type is directory
                if (Directory.Exists(path + "\\" + nfname))
                {
                    returnValue = "Directory already exists.";
                }
                else
                {
                    if (getReadWriteAccess(path, context.Request.Form["u"], alGroups).Equals("rw") || relpath.ToLower() == context.Request.Form["u"].ToLower())
                    {
                        Directory.CreateDirectory(path + "\\" + nfname);
                        returnValue = "Directory created!";
                        lochy.logIt("createFolder", context.Request.Form["u"], nfname, "OK", context.Request.Form["remotehost"]);
                    }
                    else
                    {
                        returnValue = "Access denied.";
                    }
                }
            }
            else
            {
                // type must be file
                if (File.Exists(path + "\\" + nfname))
                {
                    returnValue = "File already exists.";
                }
                else
                {
                    if (getReadWriteAccess(path, context.Request.Form["u"], alGroups).Equals("rw") || relpath.ToLower() == context.Request.Form["u"].ToLower())
                    {
                        StreamWriter sw = new StreamWriter(path + "\\" + nfname, false);
                        sw.Close();
                        returnValue = "File created.";
                        lochy.logIt("createFile", context.Request.Form["u"], nfname, "OK", context.Request.Form["remotehost"]);
                    }
                    else
                    {
                        returnValue = "Access denied.";
                    }
                }
            }
        }

        context.Response.Write(returnValue);
    }
    //----------------------------------------------------------------------------------//    

    //----------------------------------------------------------------------------------//  
    private void deleteObject(HttpContext context)
    {
        string returnValue = null;
        context.Response.ContentType = "text/plain";

        ArrayList alGroups = (ArrayList)context.Session["sesRoles"];
        LochBoxHelpers lochy = new LochBoxHelpers();

        if (lochy.verifySession(context.Request.Form["sesID"]))
        {
            string relpath = context.Server.UrlDecode(context.Request.Form["relpath"]);
            string path = lochy.BaseHomeFolderPath + relpath;
            string selectedObjectType = context.Request.Form["selectedObjectType"];

            if (getReadWriteAccess(path, context.Request.Form["u"], alGroups).Equals("rw") || relpath.ToLower() == context.Request.Form["u"].ToLower())
            {
                // if it's a folder delete it
                if (selectedObjectType == "D")
                {
                    Directory.Delete(path, true);
                }
                else
                {
                    // must be a file
                    File.Delete(path);
                }

                returnValue = "Object deleted.";

                // log this
                lochy.logIt("deleteObject", context.Request.Form["u"], path, "OK", context.Request.Form["remotehost"]);
            }
            else
            {
                returnValue = "Access denied.";
            }
        }
        else
        {
            returnValue = "INVALID_SESSION";
        }

        context.Response.Write(returnValue);
    }
    //----------------------------------------------------------------------------------//  
    
    //----------------------------------------------------------------------------------//
    private string FormatBytes(long bytes)
    {
        // return pretty size formats      
        string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
        int i = 0;
        double dblSByte = bytes;

        if (bytes > 1024)
        {
            for (i = 0; (bytes / 1024) > 0; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }
        }
        else
        {
            i = 0;
            dblSByte = bytes;
        }

        return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//    
    private string getACLasString(string path)
    {
        string returnValue = null;

        Process p = new Process();
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.UseShellExecute = false;

        p.StartInfo.FileName = "icacls";
        p.StartInfo.Arguments = "\"" + path + "\"";
        p.Start();

        // an example return (before parsed) would be like: 
        //  \\csufile1.clayton.edu\userhome\rpoore\test CCSU\jemployee:(OI)(CI)(R)
        //                                              CCSU\rpoore:(I)(OI)(CI)(F)
        //                                              CCSU\webfilemanager:(I)(OI)(CI)(F)
        //                                              CCSU\csuBackupAccount:(I)(OI)(CI)(F)
        //                                              CCSU\Domain Admins:(I)(OI)(CI)(F)        

        while (!p.StandardOutput.EndOfStream)
        {
            returnValue += p.StandardOutput.ReadLine().Replace(path, "").Replace("(OI)", "").Replace("(CI)", "").Replace("(I)", "").Replace("(IO)","").Replace("(NP)","") + ",";
        }

        p.WaitForExit();

        // this return value should be something like
        // ccsu\jemployee:(r) ccsu\rpoore:(f) ccsu\webfilemanager:(f) ccsu\csubackupaccount:(f) ccsu\domain admins:(f)successfully processed 1 files; failed processing 0 files
        
        return returnValue.ToLower();
    }
      
    
    //----------------------------------------------------------------------------------//
    private string getIcon(string strFile)
    {
        // return the right icon for the file
        string strReturnValue = null;
        string strExt = Path.GetExtension(strFile).ToLower();

        switch (strExt)
        {
            case ".jpg":
            case ".jpeg":
            case ".bmp":
            case ".gif":
            case ".png":
            case ".tiff":
                strReturnValue = "icon-picture";
                break;
            case ".doc":
            case ".docx":
                strReturnValue = "icon-file-word";
                break;
            case ".pdf":
                strReturnValue = "icon-file-pdf";
                break;
            case ".ppt":
                strReturnValue = "icon-file-powerpoint";
                break;
            case ".xls":
            case ".xlsx":
                strReturnValue = "icon-file-excel";
                break;
            case ".gz":
            case ".zip":
                strReturnValue = "icon-file-zip";
                break;
            case ".txt":
            case ".cmd":
            case ".log":
                strReturnValue = "icon-text";
                break;
            case ".htm":
            case ".html":
                strReturnValue = "icon-file-xml";
                break;
            case ".mp3":
            case ".wav":
            case ".au":
                strReturnValue = "icon-music";
                break;
            case ".mpg":
            case ".avi":
                strReturnValue = "icon-movie";
                break;
            case ".iso":
                strReturnValue = "icon-cd";
                break;
            case ".exe":
                strReturnValue = "icon-windows";
                break;
            case ".note":
                strReturnValue = "icon-article";
                break;
            default:
                strReturnValue = "icon-file-4";
                break;
        }

        return strReturnValue;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private string getReadWriteAccess(string path, string username, ArrayList alGroups)
    {
        // returns r for read only access or rw for read-write access
        string returnvalue = null;
        
        DirectoryInfo dInfo = new DirectoryInfo(path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower();
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            if ((identity.Equals("ccsu\\" + username) || alGroups.Contains(identity)) && accessType.Equals("allow"))
            {
                // user has read access at least
                returnvalue = "r";

                // found the username, now get access level 
                if (accesslevel.Contains("fullcontrol") || accesslevel.Contains("modify") || accesslevel.Contains("write") || accesslevel.Contains("createdirectories"))
                {
                    returnvalue = "rw";
                }
            }
        }

        return returnvalue;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private string GetShortenedURL(string inURL)
    {
        // returns the homemade url shortener thingy ma bob
        string shortURL = inURL;
        inURL = shortURL;

        string queryURL = "https://lochbox.clayton.edu/publicwebservices/url.asmx/getShortURL?strOriginalURL=" + inURL.Replace("https://lochbox.clayton.edu/", "");

        WebClient wc = new WebClient();
        Stream data = wc.OpenRead(queryURL);
        StreamReader reader = new StreamReader(data);
        shortURL = reader.ReadToEnd();
        data.Close();
        reader.Close();

        return shortURL;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    // look up information about a user
    private string getUserInfo(string searchfunction, string searchvalue)
    {
        DirectoryEntry deMain = new DirectoryEntry();
        deMain.Path = "LDAP://<path>";
        deMain.Username = "<super>";
        deMain.Password = "<secret>";

        // search AD for the e-mail address...
        DirectorySearcher dsMain = new DirectorySearcher(deMain);
        dsMain.SearchScope = System.DirectoryServices.SearchScope.Subtree;
        dsMain.PageSize = 500;

        SearchResult result = null;
        string returnValue = "NOT_FOUND";

        if (searchfunction == "getUserFromEmail")
        {
            dsMain.Filter = "(proxyaddresses=smtp:" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                // return the username for the account
                returnValue = result.Properties["sAMAccountname"][0].ToString();
            }
            else
            {
                returnValue = "ADDY_NOT_VALID";
            }
        }
        else if (searchfunction == "getEmailFromUsername")
        {
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                returnValue = result.Properties["mail"][0].ToString();
            }
            else
            {
                returnValue = "USERNAME_NOT_VALID";
            }
        }
        else if (searchfunction == "getEmpStuStatus")
        {
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                returnValue = result.Properties["extensionAttribute10"][0].ToString();
            }
            else
            {
                returnValue = "USERNAME_NOT_VALID";
            }
        }

        return returnValue;
    }
    //----------------------------------------------------------------------------------//   
    
    //----------------------------------------------------------------------------------//
    private bool hasWriteAccess(string username, string path)
    {
        bool returnValue = false;
        string acl = getACLasString(path);

        // check user access
        if ( acl.Contains("ccsu\\" + username + ":(f)") || acl.Contains("ccsu\\" + username + ":(m)") )
        {
            returnValue = true;
        }
        
        return returnValue;
    }
    //----------------------------------------------------------------------------------//    
    
    //----------------------------------------------------------------------------------//
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    //----------------------------------------------------------------------------------//
    
    //----------------------------------------------------------------------------------//
    private void logIt(string function, string data, string response)
    {
        // log the transactions
        MySqlConnection con = new MySqlConnection(_dbconnstring);

        string sql = "insert into accesslog (function,data,response,username,remote_host) " +
            "values ('" + function + "','" + data + "', '" + response + "', '" + _username + "','" + _remotehost + "')";

        MySqlCommand cmd = new MySqlCommand(sql, con);
        con.Open();
        cmd.ExecuteNonQuery();

        con.Close();
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private bool runCommand(string cmd)
    {
        bool blnReturn = false;

        try
        {
            ProcessStartInfo process = new ProcessStartInfo("cmd.exe", cmd);

            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            process.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(process);

            blnReturn = true;
        }
        catch (Exception x) { }

        return blnReturn;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void saveTextFile(HttpContext context)
    {
        LochBoxHelpers lochy = new LochBoxHelpers();
        
        string returnValue = null;
        string username = context.Session["sesUsername"].ToString().ToLower();
        string content = context.Request.Form["v"].ToString();
        string relpath = context.Server.UrlDecode(context.Request.Form["relpath"]);
        string path = lochy.BaseHomeFolderPath + relpath;

        ArrayList alGroups = (ArrayList)context.Session["sesRoles"];

        if (getReadWriteAccess(path, username, alGroups).Equals("rw") || relpath.ToLower() == username)
        {
            StreamWriter sw = new StreamWriter(path, false);
            sw.Write(content);

            sw.Close();

            returnValue = "File saved";
        }
        else
        {
            returnValue = "Access Denied!";
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//     
    private bool sendEmail(string strTo, string strFrom, string strSubject, string strBody)
    {
        bool blnReturn = false;

        try
        {
            MailAddress from = new MailAddress(strFrom);
            MailAddress to = new MailAddress(strTo);
            MailMessage mmEmail = new MailMessage(from, to);

            mmEmail.Subject = strSubject;
            mmEmail.Body = strBody;

            SmtpClient client = new SmtpClient("exsmtp.clayton.edu");
            client.Send(mmEmail);
            mmEmail.Dispose();
            client.Dispose();

            blnReturn = true;
        }
        catch (Exception x) { }

        return blnReturn;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void sendPublicLink(HttpContext context)
    {
        LochBoxHelpers lochy = new LochBoxHelpers();
        string subject = "A Lochbox link has been shared with you.";
        string body = context.Request.Form["v"].ToString();
        string from = context.Session["sesEmail"].ToString();
        string to = context.Request.Form["to"].ToString();

        // multiple address?
        if (to.Contains(","))
        {
            string[] addys = to.Split(',');

            foreach (string addy in addys)
            {
                lochy.sendEmail(from, addy.Trim(), subject, body);
            }
        }
        else
        {
            // just a single recipient
            lochy.sendEmail(from, to.Trim(), subject, body);
        }
    }
    //----------------------------------------------------------------------------------//  
    
    //----------------------------------------------------------------------------------//
    private string validateRecipientAccount(string searchvalue)
    {
        // search AD for the e-mail address...
        DirectorySearcher dsMain = new DirectorySearcher(deMain);
        dsMain.SearchScope = System.DirectoryServices.SearchScope.Subtree;
        dsMain.PageSize = 500;

        SearchResult result = null;
        string returnValue = "NOT_FOUND";

        if (searchvalue.Contains("@"))
            dsMain.Filter = "(proxyaddresses=smtp:" + searchvalue + ")";
        else
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";

        result = dsMain.FindOne();

        if (result != null)
            returnValue = result.Properties["sAMAccountname"][0].ToString();
        else
            returnValue = "ACCOUNT_NOT_VALID";

        return returnValue;
    }
    //----------------------------------------------------------------------------------//
    
    //----------------------------------------------------------------------------------//
    private bool verifySession(string strSesID)
    {
        bool blnVerifySession = false;
        MySqlConnection conNetwork = new MySqlConnection(_dbconnstring);
        conNetwork.Open();

        string sql = "select * from websessions where Session_ID = '" + strSesID + "'";

        MySqlCommand cmd = new MySqlCommand(sql, conNetwork);
        MySqlDataReader dr = cmd.ExecuteReader();

        if (dr.HasRows)
        {
            blnVerifySession = true;
        }

        conNetwork.Close();
        conNetwork.Dispose();

        return blnVerifySession;
    }
    //----------------------------------------------------------------------------------//
  
}