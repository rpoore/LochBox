﻿<%@ WebHandler Language="C#" Class="leaveFeedback" %>

using System;
using System.Web;
using System.Web.SessionState;

public class leaveFeedback : IHttpHandler 
{   
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/plain";
        
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("INVALID_REQUEST");
            context.Response.End();
        }

        LochBoxHelpers lochy = new LochBoxHelpers();
        
        if (lochy.verifySession(context.Request.Form["sesID"]))
        {
            string returnValue = null;
            string body = context.Request.Form["v"];
            string from = context.Request.Form["from"];
            string to = "rogerpoore@clayton.edu";
            string subject = "Lochbox Feedback";

            string response = lochy.sendEmail(from, to, subject, body);

            if (response == "SUCCESS")
                returnValue = "Your comment has been submitted.  Thank you!";
            else
                returnValue = "E-mail not sent: " + response;
                       
            context.Response.Write(returnValue);
        }
        else
        {            
            context.Response.Write("INVALID_SESSION_ID");
        }
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}