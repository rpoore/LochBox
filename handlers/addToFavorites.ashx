﻿<%@ WebHandler Language="C#" Class="addToFavorites" %>

using System;
using System.Web;

public class addToFavorites : IHttpHandler 
{   
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/plain";
        
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("INVALID_REQUEST");
            context.Response.End();
        }

        LochBoxHelpers lochy = new LochBoxHelpers();

        if (lochy.verifySession(context.Request.Form["sesID"]))        
        {
            string returnValue = null;
            
            // the relpath should always be urlencoded on the querystring
            string relpath = context.Server.UrlDecode(context.Request.Form["relpath"]);
            string absolutepath = lochy.BaseHomeFolderPath + relpath;
            string foldername = absolutepath.Substring(absolutepath.LastIndexOf(@"\") + 1);
            string destpath = lochy.BaseHomeFolderPath + context.Request.Form["u"].ToString() + "\\_Favorites\\" + foldername;
               
            try
            {
                // add a symlink to the users "_Favorites" folder..
                string cmd = "cmd /c mklink /D \"" + destpath + "\" \"" + absolutepath + "\"";

                if (lochy.runCommand(cmd))
                {
                    returnValue = "<b>" + foldername + "</b> added to your favorites.";
                    lochy.logIt("addToFavorites", context.Request.Form["u"], cmd, "OK", context.Request.Form["remotehost"]);
                }
                else
                    returnValue = "Could not start shell process!";                
            }
            catch (Exception x)
            {
                returnValue = "<b>" + foldername + "</b> was NOT added to your favorites.";
            }
                        
            context.Response.Write(returnValue);
        }
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}