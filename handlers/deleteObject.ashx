﻿<%@ WebHandler Language="C#" Class="deleteObject" %>

using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.SessionState;

public class deleteObject : IHttpHandler 
{    
    public void ProcessRequest (HttpContext context) 
    {
        string returnValue = null;
        context.Response.ContentType = "text/plain";

        // end this if it's not a proper call
        if (context.Request.UrlReferrer == null)
        {
            context.Response.Write("INVALID_REQUEST");
            context.Response.End();
        }

        ArrayList alGroups = (ArrayList)context.Session["sesRoles"];
        LochBoxHelpers lochy = new LochBoxHelpers();

        if (lochy.verifySession(context.Request.Form["sesID"]))
        {
            string relpath = context.Server.UrlDecode(context.Request.Form["relpath"]);
            string path = lochy.BaseHomeFolderPath + relpath;
            string selectedObjectType = context.Request.Form["selectedObjectType"];

            lochy.getReadWriteAccess(path, context.Request.Form["u"], alGroups);
            if (lochy.HasWriteAccess || relpath.ToLower() == context.Request.Form["u"].ToLower())  
            {
                // if it's a folder delete it
                if (selectedObjectType == "D")
                {
                    Directory.Delete(path, true);
                }
                else
                {
                    // must be a file
                    File.Delete(path);
                }

                returnValue = "Object deleted.";

                // log this
                lochy.logIt("deleteObject", context.Request.Form["u"], path, "OK", context.Request.Form["remotehost"]);                
            }
            else
            {
                returnValue = "Access denied.";                
            }            
        }
        else
        {
            returnValue = "INVALID_SESSION";
        }

        context.Response.Write(returnValue);
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}