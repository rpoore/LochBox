﻿<%@ WebHandler Language="C#" Class="userSearch" %>

using System;
using System.IO;
using System.Web;
using System.DirectoryServices;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Collections;

public class ADAccount
{
    public string id { get; set; }
    public string label { get; set; }
    public string value { get; set; }
}

public class userSearch : IHttpHandler 
{
    
    public void ProcessRequest (HttpContext context) 
    {
        string searchvalue = context.Request.QueryString["term"];

        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        string serResults = javaScriptSerializer.Serialize(getUsersList(searchvalue));
        context.Response.ContentType = "applicaton/json";
        context.Response.Write(serResults);    
    }

    private List<ADAccount> getUsersList(string searchvalue)
    {
        ADAccount _adaccount = null;
        List<ADAccount> _adaccountlist = new List<ADAccount>();
                
        DirectoryEntry deMain = new DirectoryEntry();
        deMain.Path = "<LDAP_PATH>";
        deMain.Username = "<super>";
        deMain.Password = "<secret>";

        DirectorySearcher dsMain = new DirectorySearcher(deMain);

        SortOption sort = new SortOption("displayName", SortDirection.Descending);
        dsMain.Sort = sort;        
        
        dsMain.Filter = "(&(objectClass=user)((|(givenName=" + searchvalue + "*)(sn=" + searchvalue + "*)" +
                  "(sAMAccountName=" + searchvalue + "*)(displayName=" + searchvalue + "*))))";        

        SearchResultCollection results = dsMain.FindAll();

        if (results.Count == 1)
        {
            SearchResult result = results[0];

            try
            {
                _adaccount = new ADAccount();

                _adaccount.id = result.Properties["sAMAccountName"][0].ToString();
                _adaccount.label = result.Properties["displayname"][0].ToString() + " (" + result.Properties["department"][0].ToString() + ")";
                _adaccount.value = result.Properties["sAMAccountName"][0].ToString();

                _adaccountlist.Add(_adaccount);
            }
            catch (Exception x) { }
        }
        else
        {
            foreach (SearchResult result in results)
            {
                try
                {
                    _adaccount = new ADAccount();

                    _adaccount.id = result.Properties["sAMAccountName"][0].ToString();
                    _adaccount.label = result.Properties["displayname"][0].ToString() + " (" + result.Properties["department"][0].ToString() + ")";
                    _adaccount.value = result.Properties["sAMAccountName"][0].ToString();

                    _adaccountlist.Add(_adaccount);
                }
                catch (Exception x) { }
            }
        }                
        
        return (_adaccountlist);
    }
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}