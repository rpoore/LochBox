﻿<%@ WebHandler Language="C#" Class="fileup" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using MySql.Data.MySqlClient;

public class ViewDataUploadFilesResult
{
    public string Thumbnail_url { get; set; }
    public string Name { get; set; }
    public int Length { get; set; }
    public string Type { get; set; }
}

public class fileup : IHttpHandler 
{
    
    public void ProcessRequest (HttpContext context) 
    {
        //context.Response.ContentType = "text/plain";//"application/json";
        context.Response.ContentType = "application/json";
        
        var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
        var path = @"\\csufile1.clayton.edu\userhome\" + context.Server.UrlDecode(context.Request.QueryString["relpath"].ToString());
        
        JavaScriptSerializer js = new JavaScriptSerializer();
        
        foreach (string file in context.Request.Files)
        {
            HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
            string FileName = string.Empty;
            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
            {
                string[] files = hpf.FileName.Split(new char[] { '\\' });
                FileName = files[files.Length - 1];
            }
            else
            {
                FileName = hpf.FileName;
            }
            if (hpf.ContentLength == 0)
                continue;
            string savedFileName = path + "\\" + FileName;
            hpf.SaveAs(savedFileName);

            r.Add(new ViewDataUploadFilesResult()
            {
                //Thumbnail_url = savedFileName,
                Name = FileName,
                Length = hpf.ContentLength,
                Type = hpf.ContentType
            });
            var uploadedFiles = new
            {
                files = r.ToArray()
            };
            var jsonObj = js.Serialize(uploadedFiles);
            //jsonObj.ContentEncoding = System.Text.Encoding.UTF8;
            //jsonObj.ContentType = "application/json;";
            context.Response.Write(jsonObj.ToString());
        }
    }

    private bool verifySession(string strSesID)
    {
        bool blnVerifySession = false;
        MySqlConnection conNetwork = new MySqlConnection("Persist Security Info=False;Database=network;server=mysql.clayton.edu;Port=3306;User Id=;pwd=");
        conNetwork.Open();

        string sql = "select * from webservice_sessions where wemSessionID = '" + strSesID + "'";

        MySqlCommand cmd = new MySqlCommand(sql, conNetwork);
        MySqlDataReader dr = cmd.ExecuteReader();

        if (dr.HasRows)
        {
            blnVerifySession = true;
        }

        conNetwork.Close();
        conNetwork.Dispose();

        return blnVerifySession;
    }     
 
    public bool IsReusable 
    {
        get 
        {
            return false;
        }
    }

}