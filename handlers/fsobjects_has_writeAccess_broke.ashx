﻿<%@ WebHandler Language="C#" Class="fsobjects" %>

using System;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.DirectoryServices;
using System.Web.SessionState;
using System.Security.AccessControl;
using System.Web.Configuration;
using LochBox;

public class fsobject
{
    public string NAME { get; set; }
    public string PATH { get; set; }
    public string SIZE { get; set; }
    public string LASTMODIFIED { get; set; }
    public char TYPE { get; set; }
    public string ICON { get; set; }
    public string MUSICPATH { get; set; }
}

public class ViewDataUploadFilesResult
{
    public string Thumbnail_url { get; set; }
    public string Name { get; set; }
    public int Length { get; set; }
    public string Type { get; set; }
}

public class fsobjects : IHttpHandler, IRequiresSessionState
{
    string _path = null;
    string _username = null;
    string _sesID = null;
    string _groups = null;
    string _basePath = null;
    string _baseEmployeePath = null;
    string _baseStudentPath = null;
    string _remotehost = null;
    string _dbconnstring = null;  
    string serJSON = null;
    JavaScriptSerializer _javaScriptSerializer = null;
    DirectoryEntry deMain = null;
           
    public void ProcessRequest (HttpContext context) 
    {        
        // make sure the request has the barest of essentials...
        // ------------------------------------------------------------------------------ //
        if (String.IsNullOrWhiteSpace(context.Session["sesUsername"].ToString()))
            context.Response.End();
        
        // othewerise, we continue...        
        // set the main vars
        _username = context.Session["sesUsername"].ToString();
        _sesID = context.Request.Form["sesID"];
        _groups = context.Session["sesRoles"].ToString();        
        _baseEmployeePath = @"\\csufile1.clayton.edu\userhome\";        
        _baseStudentPath = @"\\csustuweb.clayton.edu\student_home\";
        _dbconnstring = WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;        
        _javaScriptSerializer = new JavaScriptSerializer();
        _remotehost = context.Request.ServerVariables["REMOTE_HOST"].ToString();
        
        deMain = new DirectoryEntry();
        deMain.Path = "LDAP://ccsunet.clayton.edu/dc=ccsunet,dc=clayton,dc=edu";
        deMain.Username = WebConfigurationManager.AppSettings["dirreadUsername"];
        deMain.Password = WebConfigurationManager.AppSettings["dirreadPassword"];                    
        
        // show the correct path depending on student/employee status
        // ------------------------------------------------------------------------------ // 
        if (context.Session["sesUserDesc"].ToString() == "employee")
            _basePath = _baseEmployeePath;
        else
            _basePath = _baseStudentPath;
        // ------------------------------------------------------------------------------ //
                             
        // set the full path 
        // ------------------------------------------------------------------------------ // 
        if (context.Request.QueryString["relpath"] != null)
        {
            // the relpath should always be urlencoded on the querystring
            _path = _basePath + context.Server.UrlDecode(context.Request.QueryString["relpath"]);
        }
        else
            _path = _basePath + _username;
        // ------------------------------------------------------------------------------ //         
        
        // now see what needs to be done 
        // ------------------------------------------------------------------------------ // 
        if (context.Request.QueryString["q"].ToString() != null)
        {
            string query = context.Request.QueryString["q"].ToString();
      
            // clear the clipboard
            if (query == "clearclipboard")
                context.Session.Remove("sesClipboard");     
            
            // get the list of current users
            if (query == "currentusers")
                getCurrentUsers(context);  
            
            // add user to folder
            if (query == "adduser")
                addUser(context);

            // remove user from folder
            if (query == "removeuser")
                removeUser(context);              
            
            // get public link
            if (query == "getpubliclink")
                getPublicLink(context);
            
            // email public link
            if (query == "sendpublink")
                sendPublicLink(context);

            // leave feedback
            if (query == "leavefeedback")
                leaveFeedback(context);                            
            
            // stuff that depends on the path...make sure logged in user is accessing right path
            if (_path.StartsWith(_basePath + context.Session["sesUsername"].ToString()))
            {
                // list file system objects for user's path
                if (query == "list")
                    listFSObjects(context);

                // open a file
                if (query == "openfile")
                    openTextFile(context);

                // save a file
                if (query == "savefile")
                    saveTextFile(context);

                // create a file
                if (query == "createfile")
                    createTextFile(context);                                              
                
                // create a new folder
                if (query == "newfolder")
                    createFolder(context);

                // delete object
                if (query == "delobject")
                    deleteObject(context);
                
                // rename object
                if (query == "renobject")
                    renameObject(context);   
                
                // add to faves
                if (query == "addtofaves")
                    addToFavorites(context);
                
                // copy to clipboard
                if (query == "copy")
                {
                    // create session var to hold object path
                    context.Session["sesClipboard"] = context.Request.QueryString["relpath"].ToString();
                    
                    // trim the full path to just folder/file name and return it
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(context.Request.QueryString["relpath"].ToString().Substring(context.Request.QueryString["relpath"].ToString().LastIndexOf("\\")+ 1 ));                    
                }  
                
                // move object from clipboard to destination folder
                if (query == "move")
                    moveObject(context);
                
                // paste object (assuming something copied to "clipboard")
                if (query == "paste")
                    pasteObject(context); 
                
                // for file uploading
                if (query == "upload")
                    uploadFile(context);
            }            
        }
    }
    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//
    
    
    //----------------------------------------------------------------------------------//
    private void addUser(HttpContext context)
    {
        string returnValue = null;
        string userToAdd = context.Request.QueryString["usertoadd"].ToString();
        string accesslevel = context.Request.QueryString["accesslevel"].ToString();

        // get the username from the passed user and make sure it's valid
        //     can be either e-mail address or username
        string recipientaccount = validateRecipientAccount(userToAdd);

        if (recipientaccount != "ACCOUNT_NOT_VALID")
        {
            if (applyPermissions(_path, recipientaccount, accesslevel))
            {
                returnValue = "<div class='alert alert-success'>Permission granted for " + recipientaccount + "</div>";

                // get the shared folder name
                string foldername = _path.Substring(_path.LastIndexOf(@"\") + 1);

                // make the _SharedWithMe link for the recipient
                string recipientSharedWithMePath = @"D:\Websites\lochbox.clayton.edu\users\home\" + recipientaccount + @"\_SharedWithMe";

                // create the _SharedWithMe folder for the recipient if not there already
                if (!Directory.Exists(recipientSharedWithMePath))
                    Directory.CreateDirectory(recipientSharedWithMePath);

                // make the sym link 
                string symlinkname = recipientSharedWithMePath + @"\" + _username + "_" + foldername.Replace(" ", "_");

                string cmd = "/c mklink /D \"" + symlinkname + "\" \"" + _path + "\"";

                if (runCommand(cmd))
                {
                    // process did run
                    returnValue += "<div class='alert alert-success'>Symbolic link created for " + recipientaccount + "</div>";
                    logIt("makeSymLink", cmd, "SUCCESS");
                }
                else
                {
                    // did not make the symlink
                    returnValue += "<div class='alert alert-error'>Failed creating symbolic link for " + recipientaccount + "</div>";
                    logIt("makeSymLink", cmd, "FAILED");
                }
                
                // finally, e-mail the user
                //string sharedfolderurl = "https://lochbox.clayton.edu/home?type=D&relpath=" + recipientaccount + "%5c_SharedWithMe%5c" + _username + "_" + foldername.Replace(" ", "_");
                            
                string subject = "New Lochbox share...";
                string recipientEmail = getUserInfo("getEmailFromUsername", recipientaccount);
                string body = _username + " has shared \"" + foldername + "\" with you. " +
                    "You should see this folder under your \"Shared With Me\" folder after logging into " +
                    "https://lochbox.clayton.edu";

                if (sendEmail(recipientEmail, context.Session["sesEmail"].ToString(), subject, body))
                {
                    returnValue += "<div class='alert alert-success'>E-mail sent to " + recipientaccount + "</div>";
                    logIt("emailRecipient", recipientEmail, "SUCCESS");
                }
                else
                {
                    returnValue += "<div class='alert alert-error'>Failed e-mailing " + recipientaccount + "</div>";
                    logIt("emailRecipient", recipientEmail, "FAILED");
                }
            }
            else
            {
                // failed adding permission!
                returnValue = "<div class='alert alert-error'>Failed adding permission for " + recipientaccount + "</div>";
            }
        }
        else
        {
            // account not found!
            returnValue = "<div class='alert alert-error'>That's not a valid CSU account!</div>";
        }

        context.Response.ContentType = "text/html";
        context.Response.Write(returnValue);
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void createFolder(HttpContext context)
    {
        string _returnValue = null;
        string _newfolder = context.Request.QueryString["nfname"].ToString();

        if (hasWriteAccess(_username, _path))
        {
            if (Directory.Exists(_path + "\\" + _newfolder))
            {
                _returnValue = "Directory already exists.";
            }
            else
            {
                Directory.CreateDirectory(_path + "\\" + _newfolder);
                _returnValue = "Directory created!";
            }

            // log this
            logIt("createFolder", _newfolder, "OK");
        }
        else
        {
            _returnValue = "Access Denied!";
            logIt("createFolder", _newfolder, "ACCESS_DENIED");
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write(_returnValue);       
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void deleteObject(HttpContext context)
    {
        string _returnValue = null;
        string _selectedObjectType = context.Request.QueryString["selectedObjectType"].ToString();

        if (hasWriteAccess(_username, _path))
        {
            // if it's a folder delete it
            if (_selectedObjectType == "D")
            {
                Directory.Delete(_path, true);
            }
            else
            {
                // must be a file
                File.Delete(_path);
            }

            _returnValue = "Object deleted.";

            // log this
            logIt("deleteObject", _path.Replace(@"\", "/"), "OK");
        }
        else
        {
            _returnValue = "Access Denied!";
            logIt("deleteObject", _path.Replace(@"\", "/"), "ACCESS_DENIED");
        }   
        context.Response.ContentType = "text/plain";
        context.Response.Write(_returnValue);
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void renameObject(HttpContext context)
    {
        string returnValue = null;
        string selectedObjectType = context.Request.QueryString["selectedObjectType"].ToString();
        string newname = context.Request.QueryString["nfname"].ToString();

        // todo:  need to strip the folder or file name from the path
        string strippedPath = _path.Substring(0,_path.LastIndexOf(@"\"));

        if (hasWriteAccess(_username, _path))
        {
            // if it's a folder delete it
            if (selectedObjectType == "D")
            {
                Directory.Move(_path, strippedPath + @"\" + newname);
            }
            else
            {
                // must be a file
                File.Move(_path, strippedPath + @"\" + newname);
            }

            returnValue = "Object renamed.";
            //returnValue = strippedPath;

            // log this
            logIt("renameObject", _path.Replace(@"\", "/"), "OK");
        }
        else
        {
            returnValue = "Access Denied!";
            logIt("renameObject", _path.Replace(@"\", "/"), "ACCESS_DENIED");
        }  
        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }

    private void moveObject(HttpContext context)
    {
        string returnValue = null;
        string sourcePath = context.Server.UrlDecode(context.Session["sesClipboard"].ToString());
        string destPath = _basePath + context.Server.UrlDecode(context.Request.QueryString["relpath"].ToString());
        string objName = sourcePath.Substring(sourcePath.LastIndexOf(@"\") + 1);

        try
        {
            Directory.Move(sourcePath, destPath + "\\" + objName);

            // clear the session clipboard
            context.Session.Remove("sesClipboard");
            
            returnValue = "<div class='alert alert-success'>" + objName + " has been moved.</div>";
        }
        catch (Exception x)
        {
            returnValue = "<div class='alert alert-error'>Failed trying to move " + objName + ": " + x.Message + "</div>";
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);    
    }
    
    
    private void pasteObject(HttpContext context)
    {
        // object to be moved and what to do with it..
        string[] arrCopyObjectPath = (string[])context.Session["sesCopyObjectPath"];

        var whattodo = arrCopyObjectPath[0];
        var oldpath = _basePath + context.Server.UrlDecode(arrCopyObjectPath[1]);
        var newpath = _basePath + context.Server.UrlDecode(context.Request.QueryString["relpath"].ToString());
        
        // need to pull the last folder/file name only from the oldpath
        var filefolname = oldpath.Substring(oldpath.LastIndexOf(@"\"));

        if (whattodo == "cut")
        {
            Directory.Move(oldpath, newpath + "\\" + filefolname);
        }
        else if (whattodo == "copy")
        {
            // copy the stuff  
            DirectoryInfo source = new DirectoryInfo(oldpath);
            DirectoryInfo target = new DirectoryInfo(newpath);

            if (Directory.Exists(newpath) == false)
            {
                Directory.CreateDirectory(newpath);
            }  

            // Copy each file into it's new directory. 
            //foreach (FileInfo fi in source.GetFiles())
            //{
            //    Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
            //    fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            //}            
                       
        }
               
        // clear the session object containing the path to copy/cut
        context.Session.Remove("sesCopyObjectPath");
    }

    public void addToFavorites(HttpContext context)
    {
        string returnValue = null;
        string foldername = _path.Substring(_path.LastIndexOf(@"\") + 1);
        string destpath = _basePath + _username + "\\_Favorites\\" + foldername;
        
        try
        {
            // add a symlink to the users "_Favorites" folder..
            string cmd = "/c mklink /D \"" + destpath + "\" \"" + _path + "\"";

            if (runCommand(cmd))
                returnValue = "<b>" + foldername + "</b> added to your favorites.";
            else
                returnValue = "Could not start shell process!";
        }
        catch (Exception x)
        {
            returnValue = "<b>" + foldername + "</b> was NOT added to your favorites.";
        }
        
        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }     
    
    private void getCurrentUsers(HttpContext context)
    {
        string returnValue = null;

        // read the acl's and see if the user has access
        DirectoryInfo dInfo = new DirectoryInfo(_path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower().Replace("ccsu\\","");
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            if (!identity.Contains("webfilemanager") && !identity.Contains("csubackupaccount") && !identity.Contains("iusr") && !identity.Contains("domain admins") && !identity.Contains(_username))
            {
                returnValue += identity + ": " + accesslevel.Replace("modify, synchronize", "Read and Write").Replace("read, synchronize","Read");
                returnValue += " [<a href='#' class='removeuser' data-user='" + identity + "' title='Remove this user'>x</a>]<br />";
            }
        }

        context.Response.ContentType = "text/html";
        context.Response.Write(returnValue); 
    }

    private void removeUser(HttpContext context)
    {
        string returnValue = null;       
        string otheruser = context.Request.QueryString["otheruser"];
        string remotehost = context.Request.Form["remotehost"];

        fsomanager fsoman = new fsomanager();
        returnValue = fsoman.removeUser(_path, _username, _sesID, otheruser, remotehost);        

        context.Response.ContentType = "text/html";
        context.Response.Write(returnValue);
    }    

    private void listFSObjects (HttpContext context)
    {    
        DirectoryInfo dirInfo = new DirectoryInfo(_path);
        DirectoryInfo[] rgDirs = dirInfo.GetDirectories("*.*");
        FileInfo[] rgFiles = dirInfo.GetFiles("*.*");

        fsobject _fsobject = null;
        List<fsobject> _fsobjectlist = new List<fsobject>();

        // list directories first
        foreach (DirectoryInfo di in rgDirs)
        {
            // only show if the user has access
            if (hasListAccess(_username, di.FullName))
            {
                _fsobject = new fsobject();
                _fsobject.NAME = di.Name;
                _fsobject.TYPE = 'D';
                _fsobject.PATH = context.Server.UrlEncode(di.FullName.Replace(_basePath, "")); // strip the base path
                _fsobject.LASTMODIFIED = di.LastWriteTime.ToShortDateString();
                _fsobject.SIZE = "";                
                _fsobject.ICON = "icon-folder-3";

                _fsobjectlist.Add(_fsobject);
            }           
        }
            
        //list files second
        foreach (FileInfo fi in rgFiles)
        {
            // only show if the user has access
            if (hasListAccess(_username, fi.FullName))
            {
                _fsobject = new fsobject();
                _fsobject.NAME = fi.Name;
                _fsobject.TYPE = 'F';
                _fsobject.PATH = context.Server.UrlEncode(fi.FullName.Replace(_basePath, "")); // strip the base path
                _fsobject.LASTMODIFIED = fi.LastWriteTime.ToShortDateString();
                _fsobject.SIZE = FormatBytes(fi.Length);                
                _fsobject.ICON = getIcon(fi.FullName);
                //_fsobject.ISSHARED = false;

                // more of a pain to try to decode the encoded path with js
                //   so we'll just send the correct path to the client
                if (fi.Name.ToLower().EndsWith(".mp3"))
                    _fsobject.MUSICPATH = fi.FullName.Replace(_basePath, "http://lochbox.clayton.edu/users/").Replace("_Public", "").Replace("_Favorites", "").Replace("\\", "/");

                _fsobjectlist.Add(_fsobject);
            }     
        }

        // log this -- for debugging only
        //logIt("listFSObjects", _path.Replace(@"\","/"), "OK");          

        serJSON = _javaScriptSerializer.Serialize(_fsobjectlist);
        context.Response.ContentType = "applicaton/json";      
        context.Response.Write("{\"Result\":\"OK\", \"Records\": " + serJSON + " }");            
    }

    public void openTextFile(HttpContext context)
    {
        string returnValue = null;

        StreamReader input;

        input = new StreamReader(_path, System.Text.Encoding.ASCII);
        returnValue = input.ReadToEnd();
        input.Close();

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);         
    }

    public void saveTextFile(HttpContext context)
    {
        string returnValue = null;
        string content = context.Request.Form["v"].ToString();

        StreamWriter sw = new StreamWriter(_path, false);
        sw.Write(content);

        sw.Close();

        returnValue = "File saved";

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }

    public void createTextFile(HttpContext context)
    {
        string returnValue = null;
        string filename = context.Request.QueryString["filename"];

        if (File.Exists(_path + "\\" + filename))
        {
            returnValue = "File already exists.";
        }
        else
        { 
            //File.CreateText(_path + "\\" + filename);
            StreamWriter sw = new StreamWriter(_path + "\\" + filename, false);            
            sw.Close();           
            returnValue = "File created.";
        }        
        
        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);        
    }

    private void leaveFeedback(HttpContext context)
    {
        string feedback = context.Request.Form["v"];
        
        MailAddress from = new MailAddress(context.Request.Form["from"]);
        MailAddress to = new MailAddress("rogerpoore@clayton.edu");
        MailMessage mmEmail = new MailMessage(from, to);

        mmEmail.Subject = "Lochbox Feedback";
        mmEmail.Body = feedback;

        SmtpClient client = new SmtpClient("exsmtp.clayton.edu");
        client.Send(mmEmail);
        mmEmail.Dispose();
        client.Dispose();

        context.Response.ContentType = "text/plain";
        context.Response.Write("Your comment has been submitted.  Thank you!");         
    }

    public void uploadFile(HttpContext context)
    {   
        var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
        
        JavaScriptSerializer js = new JavaScriptSerializer();

        foreach (string file in context.Request.Files)
        {
            HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
            string FileName = string.Empty;
            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
            {
                string[] files = hpf.FileName.Split(new char[] { '\\' });
                FileName = files[files.Length - 1];
            }
            else
            {
                FileName = hpf.FileName;
            }
            if (hpf.ContentLength == 0)
                continue;
            //string savedFileName = _path + "\\" + FileName;
            string savedFileName = @"D:\Websites\lochbox.clayton.edu\tmpUploads\" + FileName;
            hpf.SaveAs(savedFileName);

            r.Add(new ViewDataUploadFilesResult()
            {
                //Thumbnail_url = savedFileName,
                Name = FileName,
                Length = hpf.ContentLength,
                Type = hpf.ContentType
            });
            var uploadedFiles = new
            {
                files = r.ToArray()
            };
            var jsonObj = js.Serialize(uploadedFiles);
            
            // now move this file to the right spot
            fsomanager fsoman = new fsomanager();
            fsoman.moveUploadedFile(savedFileName.Replace("D:\\", @"\\newton.clayton.edu\d$\"), _path + "\\" + FileName);
            
            context.Response.ContentType = "application/json";
            context.Response.Write(jsonObj.ToString());
        }
    }    

    public void getPublicLink(HttpContext context)
    {
        string fsPath = null;
        string returnValue = null;        

        if (context.Request.QueryString["relpath"] != null)
        {
            fsPath = context.Server.UrlDecode(context.Request.QueryString["relpath"].ToString());
            
            // make sure this is coming from the _Public folder
            if (fsPath.Contains(_username + @"\_Public\"))
            {
                // clean up the path
                // first remove the base server drive path...
                fsPath = fsPath.ToLower().Replace(_basePath, "");

                // slash switch           
                fsPath = fsPath.Replace(@"\", "/");

                // return and remove _public from the link
                returnValue = GetShortenedURL("https://lochbox.clayton.edu/users/" + fsPath.Replace("/_public", ""));
            }
            else
            {
                returnValue = "Public links only work on files under the _Public folder!";
            }
        }
        else
        {
            returnValue = "Missing fso path";
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write(returnValue);
    }

    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//
    // private functions
    //----------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//    
    public bool applyPermissions(string path, string recipientaccount, string permissionlevel)
    {
        bool applyPermissionsSuccess = false;
        string caclsSwitches = null;

        try
        {
            if (permissionlevel != "Z")
                caclsSwitches = @" /grant:r ccsu\" + recipientaccount + ":(OI)(CI)(" + permissionlevel + ") /T";
            else
                caclsSwitches = @" /T /remove:g ccsu\" + recipientaccount;

            string caclsLoc = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "ICACLS.EXE");

            Process p = new Process();

            p.StartInfo.FileName = caclsLoc;
            p.StartInfo.Arguments = "\"" + path + "\"" + caclsSwitches;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            p.StartInfo.CreateNoWindow = true;
            p.Start();

            applyPermissionsSuccess = true;
        }
        catch (Exception x)
        {
            applyPermissionsSuccess = false;
        }

        return applyPermissionsSuccess;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private string FormatBytes(long bytes)
    {
        // return pretty size formats      
        string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
        int i = 0;
        double dblSByte = bytes;

        if (bytes > 1024)
        {
            for (i = 0; (bytes / 1024) > 0; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }
        }
        else
        {
            i = 0;
            dblSByte = bytes;
        }

        return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
    }
    //----------------------------------------------------------------------------------//
    
    //----------------------------------------------------------------------------------//
    private string getIcon(string strFile)
    {
        // return the right icon for the file
        string strReturnValue = null;
        string strExt = Path.GetExtension(strFile).ToLower();

        switch (strExt)
        {
            case ".jpg":
            case ".jpeg":
            case ".bmp":
            case ".gif":
            case ".png":
            case ".tiff":
                strReturnValue = "icon-picture";
                break;
            case ".doc":
            case ".docx":
                strReturnValue = "icon-file-word";
                break;
            case ".pdf":
                strReturnValue = "icon-file-pdf";
                break;
            case ".ppt":
                strReturnValue = "icon-file-powerpoint";
                break;
            case ".xls":
            case ".xlsx":
                strReturnValue = "icon-file-excel";
                break;
            case ".gz":
            case ".zip":
                strReturnValue = "icon-file-zip";
                break;
            case ".txt":
            case ".cmd":
            case ".log":
                strReturnValue = "icon-text";
                break;
            case ".htm":
            case ".html":
                strReturnValue = "icon-file-xml";
                break;
            case ".mp3":
            case ".wav":
            case ".au":
                strReturnValue = "icon-music";
                break;
            case ".mpg":
            case ".avi":
                strReturnValue = "icon-movie";
                break;
            case ".iso":
                strReturnValue = "icon-cd";
                break;
            case ".exe":
                strReturnValue = "icon-windows";
                break;
            case ".note":
                strReturnValue = "icon-article";
                break;
            default:
                strReturnValue = "icon-file-4";
                break;
        }

        return strReturnValue;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private string GetShortenedURL(string inURL)
    {
        // returns the homemade url shortener thingy ma bob
        string shortURL = inURL;
        inURL = shortURL;

        string queryURL = "https://lochbox.clayton.edu/publicwebservices/url.asmx/getShortURL?strOriginalURL=" + inURL.Replace("https://lochbox.clayton.edu/", "");

        WebClient wc = new WebClient();
        Stream data = wc.OpenRead(queryURL);
        StreamReader reader = new StreamReader(data);
        shortURL = reader.ReadToEnd();
        data.Close();
        reader.Close();

        return shortURL;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    // look up information about a user
    private string getUserInfo(string searchfunction, string searchvalue)
    {
        DirectoryEntry deMain = new DirectoryEntry();
        deMain.Path = "LDAP://ccsunet.clayton.edu/dc=ccsunet,dc=clayton,dc=edu";
        deMain.Username = "";
        deMain.Password = "";

        // search AD for the e-mail address...
        DirectorySearcher dsMain = new DirectorySearcher(deMain);
        dsMain.SearchScope = System.DirectoryServices.SearchScope.Subtree;
        dsMain.PageSize = 500;

        SearchResult result = null;
        string returnValue = "NOT_FOUND";

        if (searchfunction == "getUserFromEmail")
        {
            dsMain.Filter = "(proxyaddresses=smtp:" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                // return the username for the account
                returnValue = result.Properties["sAMAccountname"][0].ToString();
            }
            else
            {
                returnValue = "ADDY_NOT_VALID";
            }
        }
        else if (searchfunction == "getEmailFromUsername")
        {
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                returnValue = result.Properties["mail"][0].ToString();
            }
            else
            {
                returnValue = "USERNAME_NOT_VALID";
            }
        }
        else if (searchfunction == "getEmpStuStatus")
        {
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                returnValue = result.Properties["extensionAttribute10"][0].ToString();
            }
            else
            {
                returnValue = "USERNAME_NOT_VALID";
            }
        }

        return returnValue;
    }
    //----------------------------------------------------------------------------------//   
    
    //----------------------------------------------------------------------------------//
    private bool hasListAccess(string username, string path)
    {
        bool returnValue = false;

        // read the acl's and see if the user has access
        DirectoryInfo dInfo = new DirectoryInfo(path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower();
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            // checking user accounts
            if (identity.Equals(@"ccsu\" + username) && accessType.Equals("allow"))
            {
                // okay, looks like user is allowed to at least see the object
                returnValue = true;
            }

            // checking groups            
            if (_groups.ToLower().Contains(identity.Replace("ccsu\\", "")) && accessType.Equals("allow"))
            {
                // okay, looks like user is allowed to at least see the object
                returnValue = true;
            }
        }

        return returnValue;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private bool hasWriteAccess(string username, string path)
    {
        bool returnValue = false;

        // read the acl's and see if the user has access
        DirectoryInfo dInfo = new DirectoryInfo(path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower();
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            // checking user accounts
            if (identity.Equals(@"ccsu\" + username) && ((accesslevel.Contains("fullcontrol") || accesslevel.Contains("write") || accesslevel.Contains("modify"))))
            {
                // okay, looks like user is allowed to at least see the object
                returnValue = true;
            }

            // checking groups            
            if (_groups.ToLower().Contains(identity.Replace("ccsu\\", "")) && ((accesslevel.Contains("fullcontrol") || accesslevel.Contains("write") || accesslevel.Contains("modify"))))
            {
                // okay, looks like user is allowed to at least see the object
                returnValue = true;
            }
        }

        return returnValue;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    //----------------------------------------------------------------------------------//
    
    //----------------------------------------------------------------------------------//
    private void logIt(string function, string data, string response)
    {
        // log the transactions
        MySqlConnection con = new MySqlConnection(_dbconnstring);

        string sql = "insert into accesslog (function,data,response,username,remote_host) " +
            "values ('" + function + "','" + data + "', '" + response + "', '" + _username + "','" + _remotehost + "')";

        MySqlCommand cmd = new MySqlCommand(sql, con);
        con.Open();
        cmd.ExecuteNonQuery();

        con.Close();
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private bool runCommand(string cmd)
    {
        bool blnReturn = false;

        try
        {
            ProcessStartInfo process = new ProcessStartInfo("cmd.exe", cmd);

            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            process.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(process);

            blnReturn = true;
        }
        catch (Exception x) { }

        return blnReturn;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//     
    private bool sendEmail(string strTo, string strFrom, string strSubject, string strBody)
    {
        bool blnReturn = false;

        try
        {
            MailAddress from = new MailAddress(strFrom);
            MailAddress to = new MailAddress(strTo);
            MailMessage mmEmail = new MailMessage(from, to);

            mmEmail.Subject = strSubject;
            mmEmail.Body = strBody;

            SmtpClient client = new SmtpClient("exsmtp.clayton.edu");
            client.Send(mmEmail);
            mmEmail.Dispose();
            client.Dispose();

            blnReturn = true;
        }
        catch (Exception x) { }

        return blnReturn;
    }
    //----------------------------------------------------------------------------------//

    //----------------------------------------------------------------------------------//
    private void sendPublicLink(HttpContext context)
    {
        MailAddress from = new MailAddress(context.Session["sesEmail"].ToString());
        MailAddress to = new MailAddress(context.Request.Form["to"].ToString());
        MailMessage mmEmail = new MailMessage(from, to);

        mmEmail.Subject = "A Lochbox link has been shared with you.";
        mmEmail.Body = context.Request.Form["v"].ToString();

        SmtpClient client = new SmtpClient("exsmtp.clayton.edu");
        client.Send(mmEmail);
        mmEmail.Dispose();
        client.Dispose();
    }
    //----------------------------------------------------------------------------------//  
    
    //----------------------------------------------------------------------------------//
    private string validateRecipientAccount(string searchvalue)
    {
        // search AD for the e-mail address...
        DirectorySearcher dsMain = new DirectorySearcher(deMain);
        dsMain.SearchScope = System.DirectoryServices.SearchScope.Subtree;
        dsMain.PageSize = 500;

        SearchResult result = null;
        string returnValue = "NOT_FOUND";

        if (searchvalue.Contains("@"))
            dsMain.Filter = "(proxyaddresses=smtp:" + searchvalue + ")";
        else
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";

        result = dsMain.FindOne();

        if (result != null)
            returnValue = result.Properties["sAMAccountname"][0].ToString();
        else
            returnValue = "ACCOUNT_NOT_VALID";

        return returnValue;
    }
    //----------------------------------------------------------------------------------//
    
    //----------------------------------------------------------------------------------//
    private bool verifySession(string strSesID)
    {
        bool blnVerifySession = false;
        MySqlConnection conNetwork = new MySqlConnection(_dbconnstring);
        conNetwork.Open();

        string sql = "select * from websessions where Session_ID = '" + strSesID + "'";

        MySqlCommand cmd = new MySqlCommand(sql, conNetwork);
        MySqlDataReader dr = cmd.ExecuteReader();

        if (dr.HasRows)
        {
            blnVerifySession = true;
        }

        conNetwork.Close();
        conNetwork.Dispose();

        return blnVerifySession;
    }
    //----------------------------------------------------------------------------------//
  
}