﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<%@ Import Namespace="System.DirectoryServices" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Web.SessionState" %>

<script runat="server">
    //Start Login Form Code - Assuming your are using the standard ASP.NET login control with ID="Login" and OnAuthenticate="OnAuthenticate"
    [System.Runtime.InteropServices.DllImport("advapi32.dll", SetLastError = true)]
    private static extern bool LogonUser(
        string lpszUsername,
        string lpszDomain,
        string lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        out IntPtr phToken
    );

    protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
    {
        IntPtr loginToken;
        e.Authenticated = LogonUser(this.Login1.UserName, "CCSU", this.Login1.Password, 8, 0, out loginToken);
        
        if (e.Authenticated)
        {
            LochBoxHelpers lochy = new LochBoxHelpers();
            
            this.Session["LoginToken"] = loginToken;
            Session["sesUsername"] = Login1.UserName.ToLower();
            Session["sesPasswdKey"] = Login1.Password.Crypt();

            // connect to directory and get some user info...
            DirectoryEntry deMain = new DirectoryEntry("<LDAP_PATH>");
            deMain.Username = WebConfigurationManager.AppSettings["dirreadUsername"];
            deMain.Password = WebConfigurationManager.AppSettings["dirreadPassword"]; 

            DirectorySearcher dsMain = new DirectorySearcher(deMain);
            dsMain.Filter = "(& (sAMAccountName=" + Session["sesUsername"].ToString() + ") (objectClass=user))";
            SearchResult srUser = dsMain.FindOne();
            DirectoryEntry deUser = srUser.GetDirectoryEntry();

            // now populate session variables with user info...eliminates tons of lookups        
            Session["sesEmail"] = deUser.Properties["mail"].Value.ToString();
            Session["sesFullname"] = deUser.Properties["displayName"].Value.ToString();

            // create a session id for this session
            Session["sesID"] = HttpContext.Current.Session.SessionID;

            // build the group role arraylist    
            ArrayList alGroups = new ArrayList();
            string roles = null;
                   
            foreach (object objGroup in deUser.Properties["memberOf"])
            {                
                // add each group as to the arraylist                
                string group = objGroup.ToString().ToLower().Replace("cn=",@"ccsu\");
                roles += group.Substring(0, group.IndexOf(',')) + "|";
                alGroups.Add(group.Substring(0, group.IndexOf(',')));
            }

            Session["sesRoles"] = alGroups;

            //update the session keeper with the session id
            updateSessionKeeper(Session["sesID"].ToString(), Session["sesUsername"].ToString());
            
            deUser.Close();
            deMain.Close();
            
            // log this login
            logIt("Login", "", "SUCCESS", this.Login1.UserName, Request.ServerVariables["Remote_Host"].ToString());

            Login1.DestinationPageUrl = "~/home";
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        // if the user tried to go inside the site without logging in, remember where they were going
        if (Request.QueryString["returnurl"] != null && Session["sesUsername"] != null)
            Response.Redirect(Request.QueryString["returnurl"], true);
    }

    public void updateSessionKeeper(string strSesID, string strUsername)
    {
        // track this session in a database table
        MySqlConnection con = new MySqlConnection(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);
        //string sql = "insert into websessions (Session_ID,username) values ('" + strSesID + "', '" + strUsername + "')";
        string sql = "insert into websessions (Session_ID,username) values (?Session_ID,?username)";
        con.Open();
        MySqlCommand cmd = new MySqlCommand(sql, con);
        cmd.Parameters.Add("?Session_ID", strSesID);
        cmd.Parameters.Add("?username", strUsername);        
        cmd.ExecuteNonQuery();
        con.Close();
        con.Dispose();
    }

    private void logIt(string function, string data, string response, string username, string remotehost)
    {
        // log the transactions
        MySqlConnection con = new MySqlConnection(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);

        string sql = "insert into accesslog (function,data,response,username,remote_host) " +
            "values ('" + function + "','" + data + "', '" + response + "', '" + username + "','" + remotehost + "')";

        MySqlCommand cmd = new MySqlCommand(sql, con);
        con.Open();
        cmd.ExecuteNonQuery();

        con.Close();
    }
</script>

<asp:Content ID="contentHead" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .btn { margin-top: -10px; }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            // Redirect iPhone/iPod visitors
            function isiPhone() {
                return (
                    (navigator.platform.indexOf("iPhone") != -1) ||
                    (navigator.platform.indexOf("iPod") != -1)
                );
            }

            if (isiPhone()) {
                window.location = "https://lochbox.clayton.edu/m_login.aspx";
            }

            $('#contentMain_Login1_UserName').focus();

            // press enter
            $('#contentMain_Login1_Password').keypress(function (e) {
                if (e.which == 13) {
                    $('#contentMain_Login1_LoginButton').click();
                    return false;
                }
            });

            // hide the header logout button here
            $('#login-info').hide();
            $('#username-label-header-icon').hide();
            $('#buttonStuff').hide();

            // hide the footer
            $('#pagefooter').hide();

            // handle some retarded IE issues
            if ($.browser.msie) {
                $('#contentMain_Login1_LoginButton').css("margin-top", "-28px");
                // $('#modalBrowserWarning').modal('show');
            }
        });
    </script>
</asp:Content>

<asp:Content ID="contentMain" ContentPlaceHolderID="contentMain" Runat="Server">  
        <div class="" style="">
            <div style="width:530px; margin:0 auto;">
            <asp:login id="Login1" runat="server" TitleText="" DisplayRememberMe="false" FailureText="Incorrect username or password!" 
                OnAuthenticate="OnAuthenticate" Orientation="Horizontal" TextLayout="TextOnTop" UserNameLabelText="Username:&nbsp;&nbsp;" 
                PasswordLabelText="Password:&nbsp;&nbsp;" UserNameRequiredErrorMessage="User name is required." Style="margin-bottom: 20px;">                    
                <LoginButtonStyle CssClass="btn btn-primary" />
                <FailureTextStyle  />
            </asp:login> 
            </div> 

            <div id="myCarousel" class="carousel slide" style="width:700px; margin:0 auto;">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="/resources/instructions/lochbox-instr-1.png" />
                    </div>
                    <div class="item">
                        <img src="/resources/instructions/lochbox-instr-2.png" />
                    </div>
                    <div class="item">
                        <img src="/resources/instructions/lochbox-instr-3a.png" />
                    </div>
                    <div class="item">
                        <img src="/resources/instructions/lochbox-instr-4.png" />
                    </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev"></a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next"></a>
            </div>

        </div>
</asp:Content>

