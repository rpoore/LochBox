﻿<%@ Page Language="C#" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<%@ Import Namespace="System.Web.Configuration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    private void Page_Load(object sender, System.EventArgs e)
    {
        // if the session var already expired...
        if (Session["sesUsername"] == null)
            Response.Redirect("/login?returnurl=" + Server.UrlEncode(Request.Url.ToString()), true);

        try
        {
            // remove the session id from the session keeper...
            MySqlConnection con = new MySqlConnection(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);
            string sql = "delete from websessions where Session_ID = '" + Session["sesID"].ToString() + "'";
            con.Open();
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception x) { }
        
        Session.Remove("sesUsername");
        Session.Remove("sesPasswdKey");
        Session.Remove("sesID");
        Session.Abandon();
        FormsAuthentication.SignOut();
        
        Response.Redirect("/login");
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
