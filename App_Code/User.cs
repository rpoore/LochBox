﻿using System;
using System.Collections.Generic;
using System.Web;
using System.DirectoryServices;
using System.Net.Mail;

/// <summary>
/// Summary description for user
/// </summary>
public class User
{
    public string Username;
    public string EmailAddress;
    public string EmployeeStudent;
    public string HomeFolderPath;
    private DirectoryEntry deMain = null;

    public User()
    {
        
    }

    public User(string searchvalue)
	{
        // init the object and some of its properties
        deMain = new DirectoryEntry();
        deMain.Path = "<LDAP_PATH>";
        deMain.Username = "<super>";
        deMain.Password = "<secret>";

        DirectorySearcher dsMain = new DirectorySearcher(deMain);
        dsMain.SearchScope = System.DirectoryServices.SearchScope.Subtree;
        dsMain.PageSize = 500;

        SearchResult result = null;

        // based on e-mail address or username?
        if (searchvalue.Contains("@"))
        {
            dsMain.Filter = "(proxyaddresses=smtp:" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                Username = result.Properties["sAMAccountname"][0].ToString();
                EmployeeStudent = result.Properties["extensionAttribute10"][0].ToString();
            }
            else
                Username = "USERNAME_NOT_VALID";
        }
        else
        {
            dsMain.Filter = "(sAMAccountname=" + searchvalue + ")";
            result = dsMain.FindOne();

            if (result != null)
            {
                EmailAddress = result.Properties["mail"][0].ToString();
                EmployeeStudent = result.Properties["extensionAttribute10"][0].ToString();
            }
            else
                EmailAddress = "ADDY_NOT_VALID";
        }
    }

    // get the home folder from username
    public string getHomeFolderPath(string username)
    {
        return "blah";
    }

    // send e-mail to the user
    public void sendEmail(string from, string to, string subject, string body)
    {
        MailAddress mailfrom = new MailAddress(from);
        MailAddress mailto = new MailAddress(to);
        MailMessage mmEmail = new MailMessage(mailfrom, mailto);

        mmEmail.Subject = subject;
        mmEmail.Body = body;

        SmtpClient client = new SmtpClient("exsmtp.clayton.edu");
        client.Send(mmEmail);
        mmEmail.Dispose();
        client.Dispose();
    }
    
}