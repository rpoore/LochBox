﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Text;
using System.Web;
using System.Web.Configuration;
using MySql.Data.MySqlClient;

/// <summary>
/// Summary description for LochBoxHelpers
/// </summary>

public class LochBoxHelpers
{
    private string privBaseHomeFolderPath = @"\\lochbox.clayton.edu\home\";    
    private bool hasReadAccess = false;
    private bool hasWriteAccess = false;
    private string dbconnstring = WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString; 

    public string BaseHomeFolderPath 
    {
        get { return privBaseHomeFolderPath; }
    }

    public bool HasReadAccess
    {
        get { return hasReadAccess; }
    }

    public bool HasWriteAccess
    {
        get { return hasWriteAccess; }
    }
 
    public LochBoxHelpers()
    {

    }

    #region createDBConnection method
    public MySqlConnection createDBConnection()
    {
        MySqlConnection con = new MySqlConnection(dbconnstring);

        return con;
    }
    #endregion

    #region createSpecialFolders
    public void createSpecialFolders(string path)
    {
        // _Public
        if (!Directory.Exists(path + "\\_Public"))
            Directory.CreateDirectory(path + "\\_Public");

        // _SharedWithMe
        if (!Directory.Exists(path + "\\_SharedWithMe"))
            Directory.CreateDirectory(path + "\\_SharedWithMe");

        // _Favorites
        if (!Directory.Exists(path + "\\_Favorites"))
            Directory.CreateDirectory(path + "\\_Favorites");
    }
    #endregion

    #region decryptString
    public string decryptString(string encryptpwd)
    {
        string returnValue = null;

        UTF8Encoding encodepwd = new UTF8Encoding();
        System.Text.Decoder utf8Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        returnValue = new String(decoded_char);

        return returnValue.Replace(WebConfigurationManager.AppSettings["salt"], "");
    }
    #endregion

    #region encryptString
    public string encryptString(string plaintext)
    {
        string returnValue = null;

        // add the salt
        plaintext += WebConfigurationManager.AppSettings["salt"];

        byte[] encode = new byte[plaintext.Length];

        encode = Encoding.UTF8.GetBytes(plaintext);
        returnValue = Convert.ToBase64String(encode);

        return returnValue;
    }
    #endregion

    #region fixHomeFolderPermissions
    public bool fixHomeFolderPermissions(string username)
    {
        // set home folder permissions -- in case they weren't already configured for some reason
        bool returnValue = false;

        try
        {
            string path = privBaseHomeFolderPath + username;
            string icaclsPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "ICACLS.EXE");
            string icaclsParams = path + @" /grant ccsu\" + username + ":F /T";

            if (runCommand("\"" + icaclsPath + "\"" + icaclsParams))
                returnValue = true;
            else
                returnValue = false;
        }
        catch (Exception x) { }

        return returnValue;
    }
    #endregion

    #region getADGroups
    private ArrayList getADGroups(string username)
    {
        // return an arraylist of AD groups the user belongs to
        DirectoryEntry deMain = new DirectoryEntry("<LDAP_PATH>");
        deMain.Username = WebConfigurationManager.AppSettings["dirreadUsername"];
        deMain.Password = WebConfigurationManager.AppSettings["dirreadPassword"];

        DirectorySearcher dsMain = new DirectorySearcher(deMain);
        dsMain.Filter = "(& (sAMAccountName=" + username + ") (objectClass=user))";
        SearchResult srUser = dsMain.FindOne();
        DirectoryEntry deUser = srUser.GetDirectoryEntry();

        // build the group role arraylist    
        ArrayList alGroups = new ArrayList();

        foreach (object objGroup in deUser.Properties["memberOf"])
        {
            // add each group as to the arraylist                
            string group = objGroup.ToString().ToLower().Replace("cn=", @"ccsu\");

            alGroups.Add(group.Substring(0, group.IndexOf(',')));
        }

        deUser.Close();
        deMain.Close();

        return alGroups;
    }
    #endregion

    #region getReadWriteAccess
    public void getReadWriteAccess(string path, string username, ArrayList alGroups)
    {
        //ArrayList alGroups = getADGroups(username);

        DirectoryInfo dInfo = new DirectoryInfo(path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower();
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            if ((identity.Equals("ccsu\\" + username) || alGroups.Contains(identity)) && accessType.Equals("allow"))
            {
                // user has read access at least
                hasReadAccess = true;

                // found the username, now get access level 
                if (accesslevel.Contains("fullcontrol") || accesslevel.Contains("modify") || accesslevel.Contains("write") || accesslevel.Contains("createdirectories"))
                {
                    hasWriteAccess = true;
                }
            }
        }

        //return returnValue;
    }
    #endregion

    #region logIt
    public void logIt(string function, string username, string data, string response, string remotehost)
    {
        // log the transactions
        MySqlConnection con = new MySqlConnection(dbconnstring);

        string sql = "insert into accesslog (function,data,response,username,remote_host) values (?function,?data,?response,?username,?remote_host)";

        MySqlCommand cmd = new MySqlCommand(sql, con);
        cmd.Parameters.Add("?function", function);
        cmd.Parameters.Add("?data", data);
        cmd.Parameters.Add("?response", response);
        cmd.Parameters.Add("?username", username);
        cmd.Parameters.Add("?remote_host", remotehost);        

        con.Open();
        cmd.ExecuteNonQuery();

        con.Close();
    }
    #endregion

    #region runCommand
    public bool runCommand(string cmd)
    {
        // run a shell command
        bool blnReturn = false;

        try
        {
            ProcessStartInfo process = new ProcessStartInfo("cmd.exe", cmd);

            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            process.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(process);

            blnReturn = true;
        }
        catch (Exception x) { }

        return blnReturn;
    }
    #endregion

    #region sendEmail
    public string sendEmail(string from, string to, string subject, string body)
    { 
        // send e-mail to the user
        string returnValue = null;

        try
        {
            MailAddress mailfrom = new MailAddress(from);
            MailAddress mailto = new MailAddress(to);
            MailMessage mmEmail = new MailMessage(mailfrom, mailto);

            mmEmail.Subject = subject;
            mmEmail.Body = body;

            SmtpClient client = new SmtpClient("exsmtp.clayton.edu");
            client.Send(mmEmail);
            mmEmail.Dispose();
            client.Dispose();

            returnValue = "SUCCESS";
        }
        catch (Exception x)
        {
            returnValue = x.Message;
        }

        return returnValue;
    }
    #endregion

    #region verifySession
    public bool verifySession(string strSesID)
    {
        // verify the user's session
        bool returnValue = false;
        MySqlConnection conNetwork = new MySqlConnection(dbconnstring);
        conNetwork.Open();

        string sql = "select * from websessions where Session_ID = '" + strSesID + "'";

        MySqlCommand cmd = new MySqlCommand(sql, conNetwork);
        MySqlDataReader dr = cmd.ExecuteReader();

        if (dr.HasRows)
        {
            returnValue = true;
        }

        conNetwork.Close();
        conNetwork.Dispose();

        return returnValue;
    }
    #endregion
}
