﻿<%@ Page Language="C#" %>
<%@ import Namespace="System.Security.AccessControl" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.Collections" %>

<!DOCTYPE html>

<script runat="server">
    private void Page_Load(object sender, System.EventArgs e)
    {
        //Response.Write(Session["sesRoles"].ToString());
        Response.Write("<br />");
        Response.Write("<br />");

        //string path = @"D:\Websites\lochbox.clayton.edu\users\home\rpoore\misc";
        string path = @"\\lochbox.clayton.edu\home\rpoore\_Public";

        ArrayList alGroups = (ArrayList)Session["sesRoles"];

        foreach (string group in alGroups)
        {
            Response.Write(group + "<br />");
        }
        
        DirectoryInfo dInfo = new DirectoryInfo(path);
        DirectorySecurity dSecurity = dInfo.GetAccessControl(AccessControlSections.All);

        foreach (FileSystemAccessRule drule in dSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
        {
            string identity = drule.IdentityReference.Value.ToLower();
            string accesslevel = drule.FileSystemRights.ToString().ToLower();
            string accessType = drule.AccessControlType.ToString().ToLower();

            Response.Write("<br />");
            Response.Write("Identity: " + identity);
            Response.Write("<br />");
            Response.Write("AccessLevel: " + accesslevel);
            Response.Write("<br />");
            Response.Write("AccessType: " + accessType);
            Response.Write("<br />");
            Response.Write("<br />");
        }

        Response.Write("<br /><br /><br />");

        string cmdStream = null;

        Process p = new Process();
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.UseShellExecute = false;

        p.StartInfo.FileName = "icacls";
        p.StartInfo.Arguments = "\"" + path + "\"";
        p.Start();

        while (!p.StandardOutput.EndOfStream)
        {
            cmdStream += p.StandardOutput.ReadLine().Replace(path, "").Replace("(OI)", "").Replace("(CI)", "").Replace("(I)", "");
        }

        p.WaitForExit();

        //Response.Write(cmdStream.ToLower()); 
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
